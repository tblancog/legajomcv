<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Empleado;

class EmpleadoTest extends TestCase
{
  use DatabaseMigrations;

    /**
    * Test grupo riesgo can be saved
    *
    */
    public function testCanStore()
    {
      $entity = factory(\App\Empleado::class)->make();
      $response = $this->post(route('empleados.store',
                                    $entity->toArray()) );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['nombre' => $entity->nombre]);
    }

    /**
    * Test grupo riesgo can be updated
    *
    */
    public function testCanUpdate()
    {
      $entity = factory(\App\Empleado::class)->create();
      $response = $this->put( route("empleados.update",
                                    [ "id"=> $entity->id]),
                            ['nombre' => 'Nuevo nombre'] );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['nombre' => 'Nuevo nombre']);
    }

    /**
    * Test grupo riesgo can be shown
    *
    */
    public function testCanBeShown()
    {
      $entity = factory(\App\Empleado::class)->create();
      $response = $this->get(route("empleados.show",
                                  [ "id"=> $entity->id]));
      $response->assertStatus(200);
      $response->assertJsonStructure(
        array_keys($entity->getAttributes())
      );
    }

    /**
    * Test grupo riesgo can be deleted
    *
    */
    public function testCanBeDeleted()
    {
      $entity = factory(\App\Empleado::class)->create();
      $response = $this->delete(route("empleados.destroy",
                                      ["id"=> $entity->id]));
      $response->assertStatus(200);
      $this->assertSoftDeleted($entity->getTable(),
                                       ['id' => $entity->id] );
    }
}
