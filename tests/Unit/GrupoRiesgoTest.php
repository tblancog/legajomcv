<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\GrupoRiesgo;

class GrupoRiesgoTest extends TestCase
{
  use DatabaseMigrations;

    /**
    * Test grupo riesgo can be saved
    *
    */
    public function testCanStore()
    {
      $entity = factory(GrupoRiesgo::class)->make();
      $response = $this->post(route('grupo_riesgo.store',
                                    $entity->toArray()) );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['nombre' => $entity->nombre]);
    }

    /**
    * Test grupo riesgo can be updated
    *
    */
    public function testCanUpdate()
    {
      $entity = factory(GrupoRiesgo::class)->create();
      $response = $this->put( route("grupo_riesgo.update",
                                    [ "id"=> $entity->id]),
                            ['nombre' => 'Nuevo nombre'] );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['nombre' => 'Nuevo nombre']);
    }

    /**
    * Test grupo riesgo can be shown
    *
    */
    public function testCanBeShown()
    {
      $entity = factory(GrupoRiesgo::class)->create();
      $response = $this->get(route("grupo_riesgo.show",
                                  [ "id"=> $entity->id]));
      $response->assertStatus(200);
      $response->assertJsonStructure(
        array_keys($entity->getAttributes())
      );
    }

    /**
    * Test grupo riesgo can be deleted
    *
    */
    public function testCanBeDeleted()
    {
      $entity = factory(GrupoRiesgo::class)->create();
      $response = $this->delete(route("grupo_riesgo.destroy",
                                      ["id"=> $entity->id]));
      $response->assertStatus(200);
      $this->assertSoftDeleted($entity->getTable(),
                                       ['id' => $entity->id] );
    }
}
