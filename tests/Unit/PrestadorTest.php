<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Prestador;

class PrestadorTest extends TestCase
{
  use DatabaseMigrations, WithoutMiddleware;

    /**
    * Test prestador can be saved
    *
    */
    public function testCanStore()
    {
      $entity = factory(\App\Prestador::class)->make();
      $response = $this->post(route('prestador.store',
                                    $entity->toArray()) );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['descripcion' => $entity->descripcion]);
    }

    /**
    * Test prestador can be updated
    *
    */
    public function testCanUpdate()
    {
      $entity = factory(\App\Prestador::class)->create();
      $response = $this->put( route("prestador.update",
                                    [ "id"=> $entity->id]),
                            ['descripcion' => 'Nuevo descripcion'] );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['descripcion' => 'Nuevo descripcion']);
    }

    /**
    * Test prestador can be shown
    *
    */
    public function testCanBeShown()
    {
      $entity = factory(\App\Prestador::class)->create();
      $response = $this->get(route("prestador.show",
                                  [ "id"=> $entity->id]));
      $response->assertStatus(200);
      $response->assertJsonStructure(
        array_keys($entity->getAttributes())
      );
    }

    /**
    * Test prestador can be deleted
    *
    */
    public function testCanBeDeleted()
    {
      $entity = factory(\App\Prestador::class)->create();
      $response = $this->delete(route("prestador.destroy",
                                      ["id"=> $entity->id]));
      $response->assertStatus(200);
      $this->assertSoftDeleted($entity->getTable(),
                                       ['id' => $entity->id] );
    }
}
