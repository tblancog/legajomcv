<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Elemento;

class ElementoTest extends TestCase
{
  use DatabaseMigrations, WithoutMiddleware;

    /**
    * Test elemento can be saved
    *
    */
    public function testCanStore()
    {
      $entity = factory(\App\Elemento::class)->make();
      $response = $this->post(route('elemento.store',
                                    $entity->toArray()) );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['nombre' => $entity->nombre]);
    }

    /**
    * Test elemento can be updated
    *
    */
    public function testCanUpdate()
    {
      $entity = factory(\App\Elemento::class)->create();
      $response = $this->put( route("elemento.update",
                                    [ "id"=> $entity->id]),
                            ['nombre' => 'Nuevo nombre'] );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['nombre' => 'Nuevo nombre']);
    }

    /**
    * Test elemento can be shown
    *
    */
    public function testCanBeShown()
    {
      $entity = factory(\App\Elemento::class)->create();
      $response = $this->get(route("elemento.show",
                                  [ "id"=> $entity->id]));
      $response->assertStatus(200);
      $response->assertJsonStructure(
        ['id', 'nombre']
      );
    }

    /**
    * Test elemento can be deleted
    *
    */
    public function testCanBeDeleted()
    {
      $entity = factory(\App\Elemento::class)->create();
      $response = $this->delete(route("elemento.destroy",
                                      ["id"=> $entity->id]));
      $response->assertStatus(200);
      $this->assertSoftDeleted($entity->getTable(),
                                       ['id' => $entity->id] );
    }
}
