<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\AntecedenteMedico;

class AntecedenteMedicoTest extends TestCase
{
  use DatabaseMigrations;

    /**
    * Test antecedente médico can be saved
    *
    */
    public function testCanStore()
    {
      $entity = factory(\App\AntecedenteMedico::class)->make();
      $response = $this->post(route('antecedentes.store',
                                    $entity->toArray()) );

      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['nombre' => $entity->nombre]);
    }

    /**
    * Test antecedente médico can be updated
    *
    */
    public function testCanUpdate()
    {
      $entity = factory(\App\AntecedenteMedico::class)->create();
      $response = $this->put( route("antecedentes.update",
                                    [ "id"=> $entity->id]),
                            ['nombre' => 'Nuevo nombre'] );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['nombre' => 'Nuevo nombre']);
    }

    /**
    * Test antecedente médico can be shown
    *
    */
    public function testCanBeShown()
    {
      $entity = factory(\App\AntecedenteMedico::class)->create();
      $response = $this->get(route("antecedentes.show",
                                  [ "id"=> $entity->id]));
      $response->assertStatus(200);
      $response->assertJsonStructure(
        array_keys($entity->getAttributes())
      );
    }

    /**
    * Test antecedente médico can be deleted
    *
    */
    public function testCanBeDeleted()
    {
      $entity = factory(\App\AntecedenteMedico::class)->create();
      $response = $this->delete(route("antecedentes.destroy",
                                      ["id"=> $entity->id]));
      $response->assertStatus(200);
      $this->assertSoftDeleted($entity->getTable(),
                                       ['id' => $entity->id] );
    }
}
