<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\TipoIntervencion;

class TipoIntervencionTest extends TestCase
{
  use DatabaseMigrations, WithoutMiddleware;

    /**
    * Test grupo riesgo can be saved
    *
    */
    public function testCanStore()
    {
      $entity = factory(\App\TipoIntervencion::class)->make();
      $response = $this->post(route('tipo_intervencion.store',
                                    $entity->toArray()) );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['descripcion' => $entity->descripcion]);
    }

    /**
    * Test grupo riesgo can be updated
    *
    */
    public function testCanUpdate()
    {
      $entity = factory(\App\TipoIntervencion::class)->create();
      $response = $this->put( route("tipo_intervencion.update",
                                    [ "id"=> $entity->id]),
                            ['descripcion' => 'Nuevo nombre'] );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['descripcion' => 'Nuevo nombre']);
    }

    /**
    * Test grupo riesgo can be shown
    *
    */
    public function testCanBeShown()
    {
      $entity = factory(\App\TipoIntervencion::class)->create();
      $response = $this->get(route("tipo_intervencion.show",
                                  [ "id"=> $entity->id]));
      $response->assertStatus(200);
      $response->assertJsonStructure(
        array_keys($entity->getAttributes())
      );
    }

    /**
    * Test grupo riesgo can be deleted
    *
    */
    public function testCanBeDeleted()
    {
      $entity = factory(\App\TipoIntervencion::class)->create();
      $response = $this->delete(route("tipo_intervencion.destroy",
                                      ["id"=> $entity->id]));
      $response->assertStatus(200);
      $this->assertSoftDeleted($entity->getTable(),
                                       ['id' => $entity->id] );
    }
}
