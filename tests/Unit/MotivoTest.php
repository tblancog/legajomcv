<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Motivo;

class MotivoTest extends TestCase
{
  use DatabaseMigrations, WithoutMiddleware;

    /**
    * Test motivo can be saved
    *
    */
    public function testCanStore()
    {
      $entity = factory(\App\Motivo::class)->make();
      $response = $this->post(route('motivo.store',
                                    $entity->toArray()) );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['descripcion' => $entity->descripcion]);
    }

    /**
    * Test motivo can be updated
    *
    */
    public function testCanUpdate()
    {
      $entity = factory(\App\Motivo::class)->create();
      $response = $this->put( route("motivo.update",
                                    [ "id"=> $entity->id]),
                            ['descripcion' => 'Nuevo nombre'] );
      $response->assertStatus(200);
      $this->assertDatabaseHas($entity->getTable(),
                              ['descripcion' => 'Nuevo nombre']);
    }

    /**
    * Test motivo can be shown
    *
    */
    public function testCanBeShown()
    {
      $entity = factory(\App\Motivo::class)->create();
      $response = $this->get(route("motivo.show",
                                  [ "id"=> $entity->id]));
      $response->assertStatus(200);
      $response->assertJsonStructure(
        array_keys($entity->getAttributes())
      );
    }

    /**
    * Test motivo can be deleted
    *
    */
    public function testCanBeDeleted()
    {
      $entity = factory(\App\Motivo::class)->create();
      $response = $this->delete(route("motivo.destroy",
                                      ["id"=> $entity->id]));
      $response->assertStatus(200);
      $this->assertSoftDeleted($entity->getTable(),
                                       ['id' => $entity->id] );
    }
}
