<?php
use App\TipoIntervencion;
use App\AntecedenteMedico;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });



// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route::resource('empleados', 'EmpleadoController', ['except' => ['show', 'destroy'] ] );
// Route::bind('empleado', function($id){
//   return  \App\Empleado::allTrashed()
//                             ->find($id);
// });

// Route::resource('users', 'UserController', ['except' => ['destroy'] ] );
// Route::bind('user', function($id){
//   return  \App\User::allTrashed()
//                             ->find($id);
// });

// Route::resource('antecedentes', 'AntecedenteMedicoController', ['except' => ['show', 'destroy'] ] );
// Route::bind('antecedente', function($id){
//   return  \App\AntecedenteMedico::allTrashed()
//                             ->find($id);
// });
//
// Route::resource('grupos_riesgo', 'GrupoRiesgoController', ['except' => ['show', 'destroy'],
//                                                            'parameters' => [ 'grupos_riesgo' => 'grupo' ] ] );
// Route::bind('grupo', function($id){
//   return  \App\GrupoRiesgo::allTrashed()
//                             ->find($id);
// });
//
// Route::resource('motivos', 'MotivoController', ['except' => ['show', 'destroy'] ] );
// Route::bind('motivo', function($id){
//   return  \App\Motivo::allTrashed()
//                             ->find($id);
// });
//
// Route::resource('tipo_intervencion', 'TipoIntervencionController', ['except' => ['show', 'destroy'] ] );
// Route::bind('tipo_intervencion', function($id){
//   return  \App\TipoIntervencion::allTrashed()
//                             ->find($id);
// });
//
// Route::resource('elementos', 'ElementoController', ['except' => ['show', 'destroy'] ] );
// Route::bind('elemento', function($id){
//   return  \App\Elemento::allTrashed()
//                             ->find($id);
// });
//
// Route::resource('prestadores', 'PrestadorController', [ 'except' => ['show', 'destroy'],
//                                                         'parameters'=> ['prestadores' => 'prestador']] );
// Route::bind('prestador', function($id){
//   return  \App\Prestador::allTrashed()
//                             ->find($id);
// });
