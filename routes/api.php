
<?php

use Illuminate\Http\Request;
use App\Http\Requests\AntecedenteMedicoRequest;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Antecedente Médico
Route::get('antecedentes/list', function(){
  return \App\AntecedenteMedico::withTrashed()->get(['id', 'nombre as text']);
})->middleware(['cors']);
Route::resource('antecedentes', 'AntecedenteMedicoController',
                ['except'=> ['create', 'edit', 'update', 'destroy'],
                 'middleware'=> ['cors'] ] );
Route::post('antecedentes/{antecedente}/update', 'AntecedenteMedicoController@update')
      ->name('antecedentes.update')
      ->middleware( ['cors'] );
Route::bind('antecedente', function( $id ) {
  return \App\AntecedenteMedico::withTrashed()
                                ->findOrFail($id);
});

// Grupo de Riesgo
Route::resource('grupos_riesgo', 'GrupoRiesgoController',
                ['except'=> ['create', 'edit', 'update', 'destroy'],
                'parameters' => ['grupos_riesgo' => 'grupo'],
                'middleware'=> ['cors'] ]);

Route::post('grupos_riesgo/{grupo}/update', 'GrupoRiesgoController@update')
      ->name('grupos_riesgo.update')
      ->where(['grupo'=> '[0-9]+'])
      ->middleware( ['cors'] );


Route::bind('grupo', function( $id ) {
  return \App\GrupoRiesgo::withTrashed()
                          ->findOrFail($id);
});

// Motivos
Route::prefix('motivos')->middleware(['cors'])->group( function(){
  Route::get('/', 'MotivoController@index');
  Route::post('/', 'MotivoController@store');
  Route::post('/{motivo}/update', 'MotivoController@update')->where('motivo', '[0-9]+');
  Route::get('/{motivo}', 'MotivoController@show')->where('motivo', '[0-9]+');
  Route::get('/list', 'MotivoController@list');
  Route::bind('motivo', function( $id ) {
    return \App\Motivo::withTrashed()
                        ->findOrFail($id);
  });
});


// Tipos de Intervención
Route::middleware(['cors'])->prefix('tipos_intervencion')->group( function(){
  Route::get('/', 'TipoIntervencionController@index');
  Route::post('/', 'TipoIntervencionController@store');
  Route::post('/{tipo}/update', 'TipoIntervencionController@update')->where('tipo', '[0-9]+');
  Route::get('/list', 'TipoIntervencionController@list');
  Route::get('/{tipo}', 'TipoIntervencionController@show')->where('tipo', '[0-9]+');
  Route::bind('tipo', function( $id ) {
    return \App\TipoIntervencion::withTrashed()
                                ->findOrFail($id);
  });
});
// Route::resource('tipos_intervencion', 'TipoIntervencionController',
//                 ['except'=> ['create', 'edit', 'update', 'destroy'],
//                  'parameters' => ['tipos_intervencion' => 'tipo'],
//                  'middleware'=> ['cors'] ] );
//
// Route::middleware(['cors'])->prefix('tipos_intervencion')->group( function(){
//   Route::get('/list', 'TipoIntervencionController@list');
//   Route::post('/{tipo}/update', 'TipoIntervencionController@update');
//   Route::bind('tipo', function( $id ) {
//     return \App\TipoIntervencion::withTrashed()->findOrFail($id);
//   });
// });

// Elementos Utilizados
Route::resource('elementos', 'ElementoController',
                ['except'=> ['create', 'edit', 'update', 'destroy'],
                 'middleware'=> ['cors'] ] );
Route::post('elementos/{elemento}/update', 'ElementoController@update')
     ->name('elementos.update')
     ->middleware( ['cors'] );
Route::bind('elemento', function( $id ) {
  return \App\Elemento::withTrashed()->findOrFail($id);
});

// Prestador
Route::middleware(['cors'])->prefix('prestadores')->group( function(){
  Route::get('/', 'PrestadorController@index');
  Route::post('/', 'PrestadorController@store');
  Route::post('/{prestador}/update', 'PrestadorController@update')->where('prestador', '[0-9]+');
  Route::get('/list', 'PrestadorController@list');
  Route::get('/{prestador}', 'PrestadorController@show')->where('prestador', '[0-9]+');
  Route::bind('prestador', function( $id ) {
    return \App\Prestador::withTrashed()
                        ->findOrFail($id);
  });
});

// Users
Route::get('users/{key}', 'UserController@show')->name('users.findby')->middleware(['cors']);
Route::get('users', 'UserController@index')->name('users.index')->middleware(['cors']);

Route::get('users/suggest/{q}',
          ['uses' => 'UserController@suggest'])->middleware(['cors']);
Route::get('users/search/{q}',
          ['uses' => 'UserController@search'])->middleware(['cors']);

Route::post('users', 'UserController@store')->name('users.store')->middleware(['cors']);
Route::post('users/{user}/update', 'UserController@update')
     ->name('user.update')
     ->middleware( ['cors'] );
Route::bind('user', function( $id ) {
  return \App\User::withTrashed()->findOrFail($id);
});

// Roles
Route::middleware(['cors'])->prefix('roles')->group(function(){
  Route::get('/list', 'RoleController@list');
  Route::get('/{role}/tipos', 'RoleController@tipos');
  Route::get('/{role}/tipos/{tipo_evento_id}/motivos', 'RoleController@motivos');
});


Route::get('permissions/list/{mode?}', function($mode = null){
  return \App\Permission::get(['id', 'description as text']);
})->middleware(['cors']);

// Employees
Route::middleware(['cors'])->prefix('empleados')->group( function(){
  Route::get('/', 'EmpleadoController@index')->name('empleado.index');
  Route::post('/', 'EmpleadoController@store')->name('empleado.store');
  Route::get('/{empleado}', 'EmpleadoController@show')->name('empleado.show');
  Route::patch('/{empleado}', 'EmpleadoController@update')->name('empleado.update');
});

// Events
Route::prefix('eventos')->middleware(['cors'])->group( function(){
  Route::post('/', 'EventoController@store')->name('events.store');
  Route::get('list', 'EventoController@list')->name('events.list');
  Route::get('{evento}', 'EventoController@show')->name('events.show');
  Route::bind('evento', function( $id ) {
    return \App\Evento::find($id);
  });
});

// Event types
Route::prefix('tipos_evento')->middleware(['cors'])->group( function(){
  Route::get('/list', 'TipoEventoController@list');
});


// Centros Médicos
Route::get('centros_medicos/list', function(){
  return \App\CentroMedico::get(['id', 'descripcion as text']);
})->middleware(['cors']);

// Especialidades
Route::get('especialidades/list', function(){
  return \App\EspecialidadServicio::get(['id', 'descripcion as text']);
})->middleware(['cors']);

// Usuarios derivables (médicos, psicólogos, terapistas)
Route::get('derivables/list', function(){
  return \App\Derivacion::get(['id', 'grupo as text']);
})->middleware(['cors']);

// Agentes Materiales
Route::get('agentes_materiales/list', function(){
  return \App\AgenteMaterial::getDescription()->get();
})->middleware(['cors']);

// Formas de diagnostico
Route::get('formas_diagnostico/list', function(){
  return \App\FormaDiagnostico::get(['id', 'descripcion as text']);;
})->middleware(['cors']);

// Formas de accidente
Route::get('formas_accidente/list', function(){
  return \App\FormaAccidente::get(['id', 'descripcion as text']);;
})->middleware(['cors']);

// Legajo Buscador y cruds
Route::middleware(['cors'])->prefix('legajo')->group( function(){

  Route::get('/{q}/suggestLegajos',['uses' => 'LegajoController@suggestLegajos']);

  Route::get('/{q}/suggestVacunas',['uses' => 'LegajoController@suggestVacunas']);

  Route::get('/{q}/suggestAntecedentes',['uses' => 'LegajoController@suggestAntecedentes']);


  Route::get('/{documento}/{apellido}/find',['uses' => 'LegajoController@find']);


  Route::get('/{empleado}',['uses' => 'LegajoController@datosLegajo']);



  Route::get('/{empleado}/vacunasAplicadas',['uses' => 'LegajoController@vacunasAplicadas']);


  Route::post('/{empleado}/vacunasAplicadas',['uses' => 'LegajoController@saveVacunasAplicadas']);



  Route::get('/{empleado}/antecedentes',['uses' => 'LegajoController@antecedentes']);


  Route::post('/{empleado}/antecedentes',['uses' => 'LegajoController@saveAntecedentes']);


  Route::post('/{empleado}/datosMedicos',['uses' => 'LegajoController@saveDatosMedicos']);

  Route::post('{e}/examenMedico/upload',['uses' => 'LegajoController@uploadExamenMedico']);




  Route::get('/{empleado}/direccionesAlternativas',['uses' => 'LegajoController@direccionesAlternativas']);

  Route::post('/{empleado}/direccionesAlternativas',['uses' => 'LegajoController@saveDireccionAlternativa']);


  Route::get('/{empleado}/telefonosAlternativos',['uses' => 'LegajoController@telefonosAlternativos']);

  Route::post('/{empleado}/telefonosAlternativos',['uses' => 'LegajoController@saveTelefonoAlternativo']);


});

// Diagnosticos
Route::prefix('diagnosticos')->middleware(['cors'])->group( function(){
  Route::get('abuelos', 'DiagnosticoController@abuelos')->name('diagnosticos.abuelos');
  Route::get('padres', 'DiagnosticoController@padres')->name('diagnosticos.padres');
  Route::get('hijos', 'DiagnosticoController@hijos')->name('diagnosticos.hijos');
});
