<?php

use Illuminate\Database\Seeder;
use \App\Role;
use \App\TipoEvento;
use \App\Motivo;

class PerfilTipoMotivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Roles
        $administrativo = Role::whereName('administrativo')->first()->id;
        $medico = Role::whereName('medico')->first()->id;
        //
        // // Tipos de Evento
        $consultorio = TipoEvento::whereSlug('consultorio')->first()->id;
        $domiciliaria = TipoEvento::whereSlug('domiciliaria')->first()->id;
        $periferica = TipoEvento::whereSlug('periferica')->first()->id;
        $externa = TipoEvento::whereSlug('externa')->first()->id;
        $telefonica = TipoEvento::whereSlug('telefonica')->first()->id;
        $solicitud = TipoEvento::whereSlug('solicitud')->first()->id;

        // // Motivos
        $accidente = Motivo::whereSlug('accidente-de-trabajo')->first()->id;
        $donacion = Motivo::whereSlug('donacion-de-sangre')->first()->id;
        $familiar = Motivo::whereSlug('enfermedad-familiar')->first()->id;
        $inculpable = Motivo::whereSlug('enfermedad-inculpable')->first()->id;
        $profesional = Motivo::whereSlug('enfermedad-profesional')->first()->id;
        $sinlicencia = Motivo::whereSlug('sin-licencia-medica')->first()->id;

        $data = [
            // Administrativo
            [ 'perfil_id'=> $administrativo, 'tipo_evento_id'=> $solicitud, 'motivo_id'=> $familiar ],
            [ 'perfil_id'=> $administrativo, 'tipo_evento_id'=> $solicitud, 'motivo_id'=> $inculpable ],

            // Médico
            // Consultorio
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $consultorio, 'motivo_id'=> $accidente ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $consultorio, 'motivo_id'=> $donacion ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $consultorio, 'motivo_id'=> $familiar ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $consultorio, 'motivo_id'=> $inculpable ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $consultorio, 'motivo_id'=> $profesional ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $consultorio, 'motivo_id'=> $sinlicencia ],

            // Domiciliaria
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $domiciliaria, 'motivo_id'=> $accidente ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $domiciliaria, 'motivo_id'=> $donacion ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $domiciliaria, 'motivo_id'=> $familiar ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $domiciliaria, 'motivo_id'=> $inculpable ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $domiciliaria, 'motivo_id'=> $profesional ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $domiciliaria, 'motivo_id'=> $sinlicencia ],

            // Periférica
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $periferica, 'motivo_id'=> $accidente ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $periferica, 'motivo_id'=> $donacion ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $periferica, 'motivo_id'=> $familiar ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $periferica, 'motivo_id'=> $inculpable ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $periferica, 'motivo_id'=> $profesional ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $periferica, 'motivo_id'=> $sinlicencia ],

            // Externa
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $externa, 'motivo_id'=> $accidente ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $externa, 'motivo_id'=> $donacion ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $externa, 'motivo_id'=> $familiar ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $externa, 'motivo_id'=> $inculpable ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $externa, 'motivo_id'=> $profesional ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $externa, 'motivo_id'=> $sinlicencia ],

            // Telefónica
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $telefonica, 'motivo_id'=> $accidente ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $telefonica, 'motivo_id'=> $donacion ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $telefonica, 'motivo_id'=> $familiar ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $telefonica, 'motivo_id'=> $inculpable ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $telefonica, 'motivo_id'=> $profesional ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $telefonica, 'motivo_id'=> $sinlicencia ],

            // Solicitud
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $solicitud, 'motivo_id'=> $accidente ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $solicitud, 'motivo_id'=> $donacion ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $solicitud, 'motivo_id'=> $familiar ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $solicitud, 'motivo_id'=> $inculpable ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $solicitud, 'motivo_id'=> $profesional ],
            [ 'perfil_id'=> $medico, 'tipo_evento_id'=> $solicitud, 'motivo_id'=> $sinlicencia ],
        ];

        \DB::table('perfil_tipo_motivos')->insert( $data );
    }
}
