<?php

use Illuminate\Database\Seeder;
use App\Motivo;
use Carbon\Carbon;

class MotivoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = ["Accidente de trabajo", "Donacion de sangre", "Enfermedad familiar", "Enfermedad inculpable", "Enfermedad profesional", "Sin licencia medica"];

      collect($data)->each( function($d){
        App\Motivo::create([
          "nombre" => $d,
          "slug" => str_slug($d),
          "created_at" => Carbon::now()
        ]);
      } );
    }
}
