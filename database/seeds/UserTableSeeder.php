<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users = collect([
        ["userad"=> "tblanco", "nombre"=> "Tony", "apellido"=> "Blanco", "email"=> "tblanco@cablevision.com.ar"],
        ["userad"=> "mpessina", "nombre"=> "Mariano", "apellido"=> "Pessina", "email"=> "mpessina@cablevision.com.ar"],
        ["userad"=> "maximora", "nombre"=> "Maximiliano", "apellido"=> "Mora", "email"=> "maximora@cablevision.com.ar"],

         ["userad"=> "dsanfilippo", "nombre"=> "David", "apellido"=> "Sanfilippo", "email"=> "dsanfilippo@cablevision.com.ar"]
      ]);

      $users->each(function($user) {
        $user = new \App\User($user);
        $user->role()
             ->associate(
                  \App\Role::where('name', 'medico')->first()
              )->save();
      });
    }
}
