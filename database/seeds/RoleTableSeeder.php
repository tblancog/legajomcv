<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
        ['name' => 'administrador', 'description'=> 'Administrador'],
        ['name' => 'psicologia', 'description'=> 'Psicología'],
        ['name' => 'seguridad', 'description'=> 'Seguridad'],
        ['name' => 'medico', 'description'=> 'Médico'],
        ['name' => 'terapista', 'description'=> 'Terapista'],
        ['name' => 'administrativo', 'description'=> 'Administrativo']
      ];

      collect($data)->each( function($d) {
        \App\Role::create([
          "name" => $d['name'],
          "description" => $d['description']
        ]);
      });
    }
}
