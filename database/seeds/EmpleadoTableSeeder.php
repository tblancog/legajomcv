<?php

use Illuminate\Database\Seeder;
use App\Empleado;
class EmpleadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Empleado::class, 10)->make()->each( function($e) {
          $e->save();
          $e->telefonos()->saveMany(
            factory(\App\Telefono::class, 3)->make()
          );
          $e->direcciones()->saveMany(
            factory(\App\Direccion::class, 2)->make()
          );
        });
    }
}
