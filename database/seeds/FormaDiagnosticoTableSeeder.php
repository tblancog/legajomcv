<?php

use Illuminate\Database\Seeder;

class FormaDiagnosticoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
          "Examen Periodico (R)",
          "Obra social (O)",
          "Prestaciones ART (B)",
          "Ausencia prolongada (A)"
      ];

      collect($data)->each(function($d){
        \App\FormaDiagnostico::insert([
          "descripcion" => $d
        ]);
      });
    }
}
