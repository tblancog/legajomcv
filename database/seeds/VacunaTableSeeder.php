<?php

use Illuminate\Database\Seeder;

class VacunaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
      
        "BCG",
        "Pentavalente",
        "Hepatitis A",
        "Hepatitis B",
        "Gripe",
        "Sarampión",
        "Rubéola",
        "Paperas",
        "Tos convulsa",
        "Polio",
        "VPH",
        "Neumococo",
        "Fiebre Amarilla",
        "antivaricela",
        "Rotavirus",
        "antirrábica",
        "antipoliomielítica inactivada (Salk)",
        "fiebre tifoidea",
        "Gama Globulina"
      ];
      collect($data)->each( function($d){
        \App\Vacuna::create([
          'nombre'=> $d
        ]);
      } );
    }
}
