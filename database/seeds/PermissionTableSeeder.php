<?php

use Illuminate\Database\Seeder;
use \Carbon\Carbon;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $write = [
        "Datos personales",
        "Datos médicos",
        "Antecedentes médicos",
        "Vacunas",
        "Grupos de riesgo",
        "Exposición riesgos laborales",
        "Consulta - Consultorio",
        "Consulta - Domiciliaria",
        "Consulta - Periférica",
        "Consulta - Psicológica",
        "Consulta - Terapista",
        "Carga documentación al legajo",
        "Alerta Ingreso",
        "Alerta cierre de legajo",
        "Alerta art 211",
        "Reporteria(se deberá desglosar por reporte)",
        "Solicitud ",
        "Consulta – Externa",
        "Consulta – Telefónica",
        "Back-end",
      ];

      collect($write)->each( function($item) {
        \App\Permission::insert([
          "name" => str_slug($item),
          "description" => $item,
          "created_at" => Carbon::now(),
          "updated_at" => Carbon::now()
        ]);
      });
    }
}
