<?php

use Illuminate\Database\Seeder;
use App\GrupoRiesgo;
use Carbon\Carbon;

class GrupoRiesgoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = ["Cardiovascular", "Respiratorio"];

      collect($data)->each( function($d){
        App\GrupoRiesgo::create([
          "nombre" => $d,
          "created_at" => Carbon::now()
        ]);
      } );
    }
}
