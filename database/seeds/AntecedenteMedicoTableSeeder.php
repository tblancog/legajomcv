<?php

use Illuminate\Database\Seeder;
use App\AntecedenteMedico;
use Carbon\Carbon;

class AntecedenteMedicoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = ["Abortos", "Accidente", "Antecedentes oncológicos", "cerebrovascular", "Defectos congénitos (de nacimiento)", "Diabetes", "EPOC", "Ex Fumador", "Fumador", "Hipercolesterolemia", "Hipertiroidismo", "Hipertrigliceridemia", "Hiperuricemia", "Hipotiroidismo", "No fumador", "Obesidad", "Perdida de la audición", "Perdida de la visión", "Presión arterial", "Síndrome coronario", "Tipo CA colon", "Tipo CA mama", "Tipo CA pulmón", "Tipo CA testículos", "Tipo CA tiroides"];

      collect($data)->each( function($d) {
        \App\AntecedenteMedico::insert([
          "nombre" => $d,
          "created_at" => Carbon::now()
        ]);
      });
    }
}
