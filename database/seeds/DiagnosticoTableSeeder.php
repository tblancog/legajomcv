<?php

use Illuminate\Database\Seeder;

class DiagnosticoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::unprepared( file_get_contents( resource_path('sql/abuelo_diagnosticos.sql') ));
      DB::unprepared( file_get_contents( resource_path('sql/padre_diagnosticos.sql') ));
      DB::unprepared( file_get_contents( resource_path('sql/hijo_diagnosticos.sql') ));
    }
}
