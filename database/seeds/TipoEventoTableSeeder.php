<?php

use Illuminate\Database\Seeder;

class TipoEventoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
        ['descripcion'=> 'Consultorio', 'is_event'=> 1],
        ['descripcion'=> 'Domiciliaria', 'is_event'=> 1],
        ['descripcion'=> 'Periférica', 'is_event'=> 1],
        ['descripcion'=> 'Externa', 'is_event'=> 1],
        ['descripcion'=> 'Teléfonica', 'is_event'=> 1],
        ['descripcion'=> 'Solicitud', 'is_event'=> 1],
        ['descripcion'=> 'Exámen Médico', 'is_event'=> 0]
      ];
      collect($data)->each(function($d){
        \App\TipoEvento::create([
            'descripcion'=> $d['descripcion'],
            'is_event'=> $d['is_event']
        ]);
      });
    }
}
