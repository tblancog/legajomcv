<?php

use Illuminate\Database\Seeder;
use App\TipoIntervencion;
use Carbon\Carbon;

class TipoIntervencionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = ["Adecuación de puesto", "Conflicto familiar / Social", "Conflicto personal", "Enfermedad inculpable", "Recalificación Profesional"];

      collect($data)->each( function($d){
        App\TipoIntervencion::create([
          "nombre" => $d,
          "created_at" => Carbon::now()
        ]);
      } );
    }
}
