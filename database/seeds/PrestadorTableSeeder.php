<?php

use Illuminate\Database\Seeder;
use App\Prestador;
use Carbon\Carbon;

class PrestadorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
        "Dr. Matias Sanfilippo",
        "Dr. Renzo Mora",
        "Dra. Aleja Francomana",
      ];

      collect($data)->each( function($d){
        App\Prestador::create([
          "nombre" => $d,
          "created_at" => Carbon::now()
        ]);
      } );
    }
}
