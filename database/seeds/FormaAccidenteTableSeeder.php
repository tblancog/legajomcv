<?php

use Illuminate\Database\Seeder;

class FormaAccidenteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
          ["codigo"=> "101", "descripcion"=> "CAIDAS DE PERSONAS CON DESNIVEL POR CAIDAS DESDE ALTURAS (ARBOLES, EDIFICIOS, ANDAMIOS, ESCALERAS, MAQUINAS DE TRABAJO, VEHICULOS)"],
          ["codigo"=> "102", "descripcion"=> "CAIDAS DE PERSONAS CON DESNIVEL POR CAIDAS EN PROFUNDIDADES (POZOS, FOSOS, EXCAVACIONES, ABERTURAS EN EL SUELO)"],
          ["codigo"=> "103", "descripcion"=> "CAIDAS DE PERSONAS QUE OCURREN AL MISMO NIVEL"],
          ["codigo"=> "104", "descripcion"=> "CAIDA DE PERSONAS AL AGUA"],
          ["codigo"=> "201", "descripcion"=> "DERRUMBE (CAIDAS DE MASAS DE TIERRA, DE ROCAS, DE PIEDRAS, DE NIEVE)"],
          ["codigo"=> "202", "descripcion"=> "DESPLOME (DE EDIFICIOS, DE MUROS, DE ANDAMIOS, DE ESCALERAS, DE PILAS DE MERCANCIAS)"],
          ["codigo"=> "203", "descripcion"=> "CAIDAS DE OBJETOS EN CURSO DE MANUTENCION MANUAL"],
          ["codigo"=> "204", "descripcion"=> "CAIDAS DE OBJETOS MOBILIARIOS (ARTEFACTOS DE LUZ, VENTANAS, MARCOS, BIBLIOTECAS, ETC.)"],
          ["codigo"=> "205", "descripcion"=> "OTRAS CAIDAS DE OBJETOS NO INCLUIDOS EN EPIGRAFES ANTERIORES DE ESTE APARTADO"],
          ["codigo"=> "301", "descripcion"=> "PISADAS SOBRE OBJETOS"],
          ["codigo"=> "302", "descripcion"=> "CHOQUE CONTRA OBJETOS INMOVILES (A EXCEPCION DE CHOQUES DEBIDOS A UNA CAIDA ANTERIOR)"],
          ["codigo"=> "303", "descripcion"=> "CHOQUE CONTRA OBJETOS MOVILES"],
          ["codigo"=> "304", "descripcion"=> "GOLPES POR OBJETOS MOVILES (COMPRENDIDOS LOS FRAGMENTOS VOLANTES Y LAS PARTICULAS) A EXCEPCION DE LOS GOLPES POR OBJETOS QUE CAEN"],
          ["codigo"=> "401", "descripcion"=> "ATRAPAMIENTO POR UN OBJETO"],
          ["codigo"=> "402", "descripcion"=> "ATRAPAMIENTO ENTRE UN OBJETO INMOVIL Y UN OBJETO MOVIL"],
          ["codigo"=> "403", "descripcion"=> "ATRAPAMIENTO ENTRE DOS OBJETOS MOVILES (A EXCEPCION DE LOS OBJETOS VOLANTES O QUE CAEN)"],
          ["codigo"=> "501", "descripcion"=> "ESFUERZOS FISICOS EXCESIVOS AL LEVANTAR OBJETOS"],
          ["codigo"=> "502", "descripcion"=> "ESFUERZOS FISICOS EXCESIVOS AL EMPUJAR OBJETOS"],
          ["codigo"=> "503", "descripcion"=> "ESFUERZOS FISICOS EXCESIVOS AL TIRAR DE OBJETOS"],
          ["codigo"=> "504", "descripcion"=> "ESFUERZOS FISICOS EXCESIVOS AL MANEJAR OBJETOS"],
          ["codigo"=> "505", "descripcion"=> "ESFUERZOS FISICOS EXCESIVOS AL LANZAR OBJETOS"],
          ["codigo"=> "601", "descripcion"=> "EXPOSICION AL CALOR (DE LA ATMOSFERA O DEL AMBIENTE DE TRABAJO)"],
          ["codigo"=> "602", "descripcion"=> "EXPOSICION AL FRIO (DE LA ATMOSFERA O DEL AMBIENTE DE TRABAJO)"],
          ["codigo"=> "603", "descripcion"=> "CONTACTO CON SUSTANCIAS U OBJETOS CALIENTES"],
          ["codigo"=> "604", "descripcion"=> "CONTACTO CON SUSTANCIAS U OBJETOS MUY FRIOS"],
          ["codigo"=> "605", "descripcion"=> "CONTACTO CON FUEGO"],
          ["codigo"=> "701", "descripcion"=> "EXPOSICION A LA CORRIENTE ELECTRICA (TIERRA HUMEDA, AGUA O AMBIENTE CON VAPOR QUE TRANSMITA ELECTRICIDAD)"],
          ["codigo"=> "702", "descripcion"=> "CONTACTO DIRECTO CON FUENTE DE GENERACION O TRANSMISION DE CORRIENTE ELECTRICA"],
          ["codigo"=> "801", "descripcion"=> "CONTACTO POR INHALACION DE SUSTANCIAS QUIMICAS"],
          ["codigo"=> "802", "descripcion"=> "CONTACTO POR INGESTION DE SUSTANCIAS QUIMICAS"],
          ["codigo"=> "803", "descripcion"=> "CONTACTO POR ABSORCION CUTANEA DE SUSTANCIAS QUIMICAS"],
          ["codigo"=> "804", "descripcion"=> "CONTACTO CON AGENTES BIOLOGICOS (ABSORCION, INHALACION)"],
          ["codigo"=> "805", "descripcion"=> "EXPOSICION A RADIACIONES IONIZANTES"],
          ["codigo"=> "806", "descripcion"=> "EXPOSICION A OTRAS RADIACIONES"],
          ["codigo"=> "807", "descripcion"=> "INOCULACION DE AGENTES BIOLOGICOS (POR PINCHAZO, HERIDAS CORTANTES)"],
          ["codigo"=> "901", "descripcion"=> "EXPLOSION O IMPLOSION"],
          ["codigo"=> "902", "descripcion"=> "INCENDIO"],
          ["codigo"=> "903", "descripcion"=> "ATROPELLAMIENTO DE ANIMALES"],
          ["codigo"=> "904", "descripcion"=> "MORDEDURA DE ANIMALES"],
          ["codigo"=> "905", "descripcion"=> "PICADURAS"],
          ["codigo"=> "906", "descripcion"=> "ATROPELLAMIENTO POR VEHICULO"],
          ["codigo"=> "907", "descripcion"=> "CHOQUE DE VEHICULOS"],
          ["codigo"=> "908", "descripcion"=> "FALLAS EN LOS MECANISMOS PARA TRABAJOS HIPERBARICOS"],
          ["codigo"=> "909", "descripcion"=> "AGRESION CON ARMAS"],
          ["codigo"=> "910", "descripcion"=> "AGRESION SIN ARMAS"],
          ["codigo"=> "911", "descripcion"=> "INJURIA PUNZO-CORTANTE O CONTUSA INVOLUNTARIA"],
          ["codigo"=> "999", "descripcion"=> "OTRAS FORMAS DE ACCIDENTE NO INCLUIDAS EN LA PRESENTE CODIFICACION"]
      ];

      collect($data)->each(function($d){
        \App\FormaAccidente::insert([
          "codigo" => $d["codigo"],
          "descripcion" => $d["descripcion"]
        ]);
      });
    }
}
