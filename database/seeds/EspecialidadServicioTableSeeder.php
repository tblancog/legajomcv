<?php

use Illuminate\Database\Seeder;

class EspecialidadServicioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\EspecialidadServicio::class, 5)->create();
    }
}
