<?php

use Illuminate\Database\Seeder;
use App\Elemento;
use Carbon\Carbon;

class ElementoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $data = [ "Cinta", "Curitas", "Ibuprofeno 500 capsula blanda", "Tafirol 400 pastilla" ];

      collect($data)->each( function($d){
        App\Elemento::create([
          "nombre" => $d,
          "created_at" => Carbon::now()
        ]);
      } );
    }
}
