<?php

use Illuminate\Database\Seeder;

class DerivacionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'medicos',
          'psicologos',
          'terapistas'
        ];
        collect($data)->each(function($d){
          \App\Derivacion::create([
            'grupo'=> $d
          ]);
        });
    }
}
