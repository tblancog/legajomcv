<?php

use Illuminate\Database\Seeder;

class CentroMedicoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $data = [
        'Fitz Roy',
        'MLE',
        'Astrolaboral',
        'IRT'
      ];
      collect($data)->each( function($d){
        \App\CentroMedico::create([
          'descripcion'=> $d
        ]);
      } );
    }
}
