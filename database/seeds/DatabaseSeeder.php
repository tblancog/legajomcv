<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmpleadoTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(AntecedenteMedicoTableSeeder::class);
        $this->call(MotivoTableSeeder::class);
        $this->call(TipoIntervencionTableSeeder::class);
        $this->call(GrupoRiesgoTableSeeder::class);
        $this->call(ElementoTableSeeder::class);
        $this->call(PrestadorTableSeeder::class);
        $this->call(AgenteMaterialTableSeeder::class);
        $this->call(TipoEventoTableSeeder::class);
        $this->call(CentroMedicoTableSeeder::class);
        $this->call(EspecialidadServicioTableSeeder::class);
        $this->call(DerivacionTableSeeder::class);
        $this->call(FormaAccidenteTableSeeder::class);
        $this->call(FormaDiagnosticoTableSeeder::class);
        $this->call(VacunaTableSeeder::class);
        $this->call(PerfilTipoMotivoSeeder::class);

       // $this->call(DiagnosticoTableSeeder::class);
    }
}
