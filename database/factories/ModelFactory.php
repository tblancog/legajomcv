<?php
use App\Elemento;
use App\Empleado;
use App\Derivacion;
use App\TipoEvento;
use App\GrupoRiesgo;
use App\EstadoEvento;
use App\AgenteMaterial;
use App\FormaDiagnostico;
use App\TipoIntervencion;
use App\AntecedenteMedico;
use App\EspecialidadServicio;
use Faker\Generator;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

/**
* Factory for User entity
*
*/
$factory->define(App\User::class, function (Faker\Generator $faker) use ($factory) {

    return [
        'role_id' => \App\Role::all()->id,
        'userad' => $faker->unique()->userName,
        'nombre' =>   $faker->firstName,
        'apellido' => $faker->lastName,
        'email' => $faker->unique()->email,
        'sector' => $faker->word,
        'documento' => $faker->numberBetween($min = 1000000, $max = 100000000),
        'cuil' => $faker->numberBetween($min = 1000000, $max = 100000000),
        'legajo' => $faker->numberBetween($min = 1000, $max = 10000),
        'puesto' => $faker->word,
        'gerencia' => $faker->word,
        'matricula' => $faker->numberBetween($min = 1000, $max = 100000),
        'firma' => $faker->word,
    ];
  });

  /**
  * Factory for Role entity
  *
  */
  $factory->define(\App\Role::class, function(Faker\Generator $faker) {

    $randomRole = $faker->randomElement(['Administrador', 'Psicólogo', 'Seguridad',
                                          'Médico', 'Terapista', 'Adminstrativo']);
    return [
      'name'=> str_slug($randomRole),
      'description'=> $randomRole
    ];
  });

  /**
  * Factory for Permission entity
  *
  */
  $factory->define(\App\Permission::class, function(Faker\Generator $faker) {
    return [
      'name'=> $faker->randomElement(['create_users', 'create_motivos', 'show_antecedentes', 'show_grupos_riesgo']),
      'description'=> $faker->word
    ];
  });


/**
* Factory for Empleado entity
*
*/
$factory->define(App\Empleado::class, function (Faker\Generator $faker) {

  return [
    'foto' =>     $faker->imageUrl(50, 50, 'cats'),
    'nombre' =>   $faker->firstName,
    'apellido' => $faker->lastName,
    'dni' =>      $faker->numberBetween($min = 1000000, $max = 100000000),
    'cuil' =>     $faker->numberBetween($min = 1000000, $max = 100000000),
    'genero' =>   $faker->randomElement($array = ['m', 'f'], $count = 1),
    'nacionalidad' => $faker->word,
    'estadocivil'=> $faker->randomElement($array = ['soltero', 'casado', 'divorciado', 'viudo']),
    'fecha_ingreso' => $faker->date($format = 'Y-m-d', $max = 'now'),
    'puesto' => $faker->word,
    'gerencia' => $faker->streetAddress,
    'region' => $faker->streetAddress,
    'subregion' => $faker->streetAddress,
    'perfil' => $faker->word,
    'jefe_directo' => $faker->name,
    'referente_rrhh' => $faker->name,
    'encuadre' => $faker->word,
    'carga_familia' => $faker->boolean,
    'art' => $faker->word,
    'obra_social_prepaga' => $faker->word,
    'empresa' => $faker->word,
    'domicilio_laboral' => $faker->streetAddress,
  ];

});

/**
* Factory for Empleado entity
*
*/
$factory->define(App\AntecedenteMedico::class, function (Faker\Generator $faker) {

  return [
      'nombre' => $faker->unique()->word,
  ];
});

/**
* Factory for Grupo de riesgo entity
*
*/
$factory->define(App\GrupoRiesgo::class, function (Faker\Generator $faker) {

  return [
      'nombre' => $faker->unique()->word,
  ];
});

/**
* Factory for Motivo entity
*
*/
$factory->define(App\Motivo::class, function (Faker\Generator $faker) {

  return [
      'nombre' => $faker->unique()->word,
  ];
});

/**
* Factory for CreateTipoIntervencion entity
*
*/
$factory->define(App\TipoIntervencion::class, function (Faker\Generator $faker) {

  return [
      'nombre' => $faker->unique()->word,
  ];
});

/**
* Factory for Elementos entity
*
*/
$factory->define(App\Elemento::class, function (Faker\Generator $faker) {

  return [
      'nombre' => $faker->unique()->word,
  ];
});

/**
* Factory for Prestador entity
*
*/
$factory->define(App\Prestador::class, function (Faker\Generator $faker) {

  return [
      'nombre' => $faker->unique()->word,
  ];
});

/**
* Factory for Telefono entity
*
*/
$factory->define(App\Telefono::class, function (Faker\Generator $faker) {

  return [
      'numero' => $faker->unique()->phoneNumber,
      'descripcion'=> $faker->randomElement($array = ['principal', 'alternativo'])
  ];
});

/**
* Factory for Direccion entity
*
*/
$factory->define(App\Direccion::class, function (Faker\Generator $faker) {

  return [
      'direccion'=> $faker->address,
      'descripcion'=> $faker->randomElement($array = ['principal', 'alternativa'])
  ];
});

/**
* Factory for Evento entity
*
*/
$factory->define(App\Evento::class, function(\Faker\Generator $faker) use ($factory) {

  return [
    'user_id' => \App\User::all()->random()->id,
    'consultorio' => $faker->word,
    'motivo_id' => \App\Motivo::all()->random()->id,
    'fecha_consulta' => $faker->date($format = 'Y-m-d', $max = 'now'),
    // Diagnosticos
    'tipo_diagnostico' => $faker->randomElement(['CIE10', 'DSM 5']),
    'diagnostico_abuelo_id' => $faker->randomNumber(1),
    'diagnostico_padre_id' => $faker->randomNumber(1),
    'diagnostico_hijo_id' => $faker->randomNumber(1),

    'evolucion' => $faker->realText($maxNbChars = 100, $indexSize = 2),
    // 'elemento_id' => \App\Elemento::all()->random()->id,
    'elemento_id' => \App\Elemento::all()->random()->id,
    // 'derivacion_id' => \App\Derivacion::all()->random()->id,
    'derivacion_id' => \App\Derivacion::all()->random()->id,

    'ausentismo' => $faker->realText($maxNbChars = 50, $indexSize = 2),
    'dias_ausentismo' => $faker->numerify( '## días' ),
    'tipo_intervencion_id' => \App\TipoIntervencion::all()->random()->id,
    'informeinicial' => $faker->realText($maxNbChars = 100, $indexSize = 2),
    'n_carpeta' => $faker->numerify( '###' ),

    'detalle_accion'=> $faker->word,
    'fecha_accion' => $faker->date($format = 'Y-m-d', $max = 'now'),

    // 'estado_id' => \App\EstadoEvento::all()->random()->id,
    'estado_id' => \App\EstadoEvento::all()->random()->id,
    // 'empleado_id' => \App\Empleado::all()->random()->id,
    'empleado_id' => \App\Empleado::all()->random()->id,

    'n_siniestro' => $faker->numerify( '###' ),
    'adjunto' => $faker->imageUrl($width = 640, $height = 480),
    'prestador_id' => \App\Prestador::all()->random()->id,
    'interconsulta' => $faker->word,

    // 'centromedico_id' => \App\CentroMedico::all()->random()->id,
    'centromedico_id' => \App\CentroMedico::all()->random()->id,
    // 'especialidadservicio_id' => \App\EspecialidadServicio::all()->random()->id,
    'especialidadservicio_id' => \App\EspecialidadServicio::all()->random()->id,
    'horajornadaaccidente' => Carbon::now()->format('Y-m-d H:i:s'),
    'inicioinasistencialaboral' => Carbon::now()->format('Y-m-d H:i:s'),
    'tareahabitualalaccidente' => $faker->boolean,

    'calleocurrenciaaccidente' => $faker->streetAddress,
    // 'agentematerial_id' => \App\AgenteMaterial::all()->random()->id,
    'agentematerial_id' => \App\AgenteMaterial::all()->random()->id,
    'tiempoexposicionagente'=> $faker->randomNumber,
    // 'formadiagnostico_id' => \App\FormaDiagnostico::all()->random()->id,
    'formadiagnostico_id' => \App\FormaDiagnostico::all()->random()->id,
    'solicitante' => $faker->name,
    'familiar' => $faker->word,
  ];
});

/**
* Factory for TipoEvento entity
*
*/
$factory->define(App\TipoEvento::class, function(\Faker\Generator $faker){
  return [
    'descripcion' => $faker->word
  ];
});

/**
* Factory for Derivacion entity
*
*/
$factory->define(App\Derivacion::class, function(\Faker\Generator $faker){
  return [
    'grupo' => $faker->randomElement(['medicos', 'psicologos', 'terapistas'])
  ];
});

/**
* Factory for EstadoEvento entity
*
*/
$factory->define(App\EstadoEvento::class, function(\Faker\Generator $faker){
  return [
    'descripcion' => $faker->numerify('Estado #')
  ];
});

/**
* Factory for CentroMedico entity
*
*/
$factory->define(App\CentroMedico::class, function(\Faker\Generator $faker){
  return [
    'descripcion' => $faker->numerify('Centro #')
  ];
});

/**
* Factory for EspecialidadServicio entity
*
*/
$factory->define(App\EspecialidadServicio::class, function(\Faker\Generator $faker){
  return [
    'descripcion' => $faker->numerify('Especialidad #')
  ];
});

/**
* Factory for AgenteMaterial entity
*
*/
$factory->define(App\AgenteMaterial::class, function(\Faker\Generator $faker){
  return [
    'codigo' => $faker->randomNumber(),
    'descripcion' => $faker->numerify('Agente #')
  ];
});

/**
* Factory for FormaDiagnostico entity
*
*/
$factory->define(App\FormaDiagnostico::class, function(\Faker\Generator $faker){
  return [
    'descripcion' => $faker->numerify('Forma diagnóstico #')
  ];
});

/**
* Factory for FormaAccidente entity
*
*/
$factory->define(App\FormaAccidente::class, function(\Faker\Generator $faker){
  return [
    'descripcion' => $faker->numerify('Forma accidente #')
  ];
});
