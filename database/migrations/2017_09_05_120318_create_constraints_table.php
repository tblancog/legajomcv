<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstraintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


      Schema::table('antecedentes_grupo_riesgo', function(BluePrint $table) {

        $table->foreign('antecedente_id')
              ->references('id')
              ->on('antecedentes_medicos');

        $table->foreign('grupo_riesgo_id')
              ->references('id')
              ->on('grupos_riesgo');
      });


      Schema::table('user_permissions', function(Blueprint $table) {
          $table->foreign('user_id')
                ->references('id')
                ->on('users');
          $table->foreign('permission_id')
                ->references('id')
                ->on('permissions');

          $table->boolean('is_readonly');
      });

      Schema::table('role_permissions', function(Blueprint $table) {
          $table->foreign('role_id')
                ->references('id')
                ->on('roles');
          $table->foreign('permission_id')
                ->references('id')
                ->on('permissions');

      });


      Schema::table('users', function (Blueprint $table) {
          $table->foreign('role_id')
                ->references('id')
                ->on('roles');
      });
      Schema::table('telefonos', function (Blueprint $table) {
          $table->foreign('empleado_id')
                ->references('id')
                ->on('empleados');
      });

      Schema::table('direcciones', function (Blueprint $table) {
          $table->foreign('empleado_id')
                ->references('id')
                ->on('empleados');
      });


      Schema::table('datos_medicos', function (Blueprint $table) {
         $table->foreign('empleado_id')
                ->references('id')
                ->on('empleados');
      });



      Schema::table('datos_medicos_vacunas', function (Blueprint $table) {
         $table->foreign('empleado_id')
                ->references('id')
                ->on('empleados');

         $table->foreign('vacuna_id')
                ->references('id')
                ->on('vacunas');
      });

      Schema::table('datos_medicos_antecedentes', function (Blueprint $table) {

         $table->foreign('empleado_id')
                ->references('id')
                ->on('empleados');

         $table->foreign('antecedente_id')
                ->references('id')
                ->on('antecedentes_medicos');
      });

      Schema::table('eventos', function (Blueprint $table) {
        $table->foreign('tipo_evento_id')
              ->references('id')
              ->on('tipos_evento');
        $table->foreign('user_id')
              ->references('id')
              ->on('users');

        $table->foreign('motivo_id')
              ->references('id')
              ->on('motivos');
        $table->foreign('elemento_id')
              ->references('id')
              ->on('elementos');

        $table->foreign('derivacion_id')
              ->references('id')
              ->on('derivaciones');
        $table->foreign('tipo_intervencion_id')
              ->references('id')
              ->on('tipos_intervencion');

        $table->foreign('estado_id')
              ->references('id')
              ->on('estados_evento');
        $table->foreign('empleado_id')
              ->references('id')
              ->on('empleados');

        $table->foreign('centromedico_id')
              ->references('id')
              ->on('centros_medicos');
        $table->foreign('especialidadservicio_id')
              ->references('id')
              ->on('empleados');

        $table->foreign('agentematerial_id')
              ->references('id')
              ->on('agentes_materiales');
        $table->foreign('formadiagnostico_id')
              ->references('id')
              ->on('formas_diagnostico');

        $table->foreign('forma_accidente_id')
              ->references('id')
              ->on('formas_accidente');
      });

      Schema::table('eventos_archivo', function (Blueprint $table) {
        $table->foreign('evento_id')
              ->references('id')
              ->on('eventos');
      });

      Schema::table('padre_diagnosticos', function (Blueprint $table) {
        $table->foreign('abuelo_id')
              ->references('id')
              ->on('abuelo_diagnosticos');
      });

      Schema::table('hijo_diagnosticos', function (Blueprint $table) {
        $table->foreign('padre_id')
              ->references('id')
              ->on('padre_diagnosticos');
      });

      Schema::table('perfil_tipo_motivos', function(Blueprint $table) {
        $table->foreign('perfil_id')
              ->references('id')
              ->on('roles');
        $table->foreign('tipo_evento_id')
              ->references('id')
              ->on('tipos_evento');
        $table->foreign('motivo_id')
              ->references('id')
              ->on('motivos');
      });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0');

      Schema::dropIfExists('perfil_tipo_motivos');
      Schema::dropIfExists('motivos');
      Schema::dropIfExists('vacunas');
      Schema::dropIfExists('grupos_riesgo');
      Schema::dropIfExists('tipos_intervencion');
      Schema::dropIfExists('elementos');
      Schema::dropIfExists('prestadores');
      Schema::dropIfExists('antecedentes_grupo_riesgo');
      Schema::dropIfExists('antecedentes_medicos');
      Schema::dropIfExists('users');
      Schema::dropIfExists('roles');
      Schema::dropIfExists('permissions');
      Schema::dropIfExists('user_permissions');
      Schema::dropIfExists('role_permissions');
      Schema::dropIfExists('telefonos');
      Schema::dropIfExists('direcciones');



      Schema::dropIfExists('datos_medicos');
      Schema::dropIfExists('datos_medicos_vacunas');
      Schema::dropIfExists('datos_medicos_antecedentes');

      Schema::dropIfExists('empleados');


      Schema::dropIfExists('tipos_evento');
      Schema::dropIfExists('eventos_archivo');

      Schema::dropIfExists('eventos');
      Schema::dropIfExists('derivaciones');
      Schema::dropIfExists('estados_evento');
      Schema::dropIfExists('centros_medicos');
      Schema::dropIfExists('especialidad_servicios');
      Schema::dropIfExists('agentes_materiales');
      Schema::dropIfExists('formas_diagnostico');

      Schema::dropIfExists('hijo_diagnosticos');
      Schema::dropIfExists('padre_diagnosticos');
      Schema::dropIfExists('abuelo_diagnosticos');
      Schema::dropIfExists('formas_accidente');

      DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
