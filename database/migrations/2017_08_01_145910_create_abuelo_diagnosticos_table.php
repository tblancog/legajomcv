<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbueloDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abuelo_diagnosticos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('codigo', 10)->unique();
          $table->text('descripcion', 255);
          $table->enum('tipo', ['CIE10', 'DSM5']);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
