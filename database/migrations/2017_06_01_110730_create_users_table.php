<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('role_id')->unsigned()->index();

          $table->string('userad', 45)->unique()->nullable();
          $table->string('nombre', 45);
          $table->string('apellido', 45);
          $table->string('email', 45)->unique();
          $table->string('sector', 45)->nullable();
          $table->string('documento', 45)->nullable();
          $table->string('cuil', 45)->nullable();
          $table->string('puesto', 45)->nullable();
          $table->string('legajo', 45)->nullable();
          $table->string('gerencia', 45)->nullable();
          $table->string('matricula', 45)->nullable();
          $table->string('firma', 45)->nullable();

          $table->softDeletes();
          $table->timestamps();
        });

        Schema::create('user_permissions', function (Blueprint $table) {
          $table->integer('user_id') ->unsigned();
          $table->integer('permission_id') ->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
