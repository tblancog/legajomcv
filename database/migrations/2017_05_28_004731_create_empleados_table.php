<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
          $table->increments('id');
          $table->string('foto')->nullable();
          $table->string('nombre')->nullable();
          $table->string('apellido')->nullable();
          $table->string('dni')->nullable();
          $table->string('cuil')->nullable();
          $table->string('legajo')->nullable();
          $table->string('genero')->nullable();
          $table->string('nacionalidad')->nullable();
          $table->string('estadocivil')->nullable();
          $table->date('fecha_ingreso')->nullable();
          $table->date('fecha_nacimiento')->nullable();
          $table->string('codigo_puesto')->nullable();
          $table->string('puesto')->nullable();
          $table->string('gerencia')->nullable();
          $table->string('region')->nullable();
          $table->string('subregion')->nullable();
          $table->string('perfil')->nullable();
          $table->string('jefe_directo')->nullable();
          $table->string('referente_rrhh')->nullable();
          $table->string('encuadre')->nullable();
          $table->string('carga_familia')->nullable();
          $table->string('art')->nullable();
          $table->string('obra_social_prepaga')->nullable();
          $table->string('empresa')->nullable();
          $table->string('domicilio_laboral')->nullable();
          $table->string('telefono_laboral')->nullable();


          $table->enum('tipo',['POSTULANTE','NOMINA','EXTERNO'])->default('POSTULANTE');

          $table->softDeletes();
          $table->timestamps();
        });





    
    

    
        Schema::create('datos_medicos', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('empleado_id')->unsigned()->index();
          $table->string('peso')->nullable();
          $table->string('altura')->nullable();
          $table->string('miembro_superhabil')->nullable();
           $table->string('grupo_sanguineo')->nullable();
          $table->boolean('dador_sangre')->nullable();
          $table->boolean('donante_organos')->nullable();
          $table->boolean('certificado_discapacidad')->nullable();

          $table->string('tipo_discapacidad')->nullable(false);
          $table->boolean('exposicion_riesgo_laboral')->default(false);
          $table->softDeletes();

          $table->timestamps();
        });


        Schema::create('datos_medicos_vacunas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('empleado_id')->unsigned()->index();
          $table->integer('vacuna_id')->unsigned()->index();
          $table->timestamps();

          
        });


        Schema::create('datos_medicos_antecedentes', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('empleado_id')->unsigned()->index();
          $table->integer('antecedente_id')->unsigned()->index();
          $table->timestamps();

          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
