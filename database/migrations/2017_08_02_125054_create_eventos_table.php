<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_evento_id')->unsigned();
            $table->integer('user_id')->unsigned()->index();
            $table->string('consultorio')->nullable();
            $table->integer('motivo_id')->nullable()->unsigned()->index();
            $table->date('fecha_consulta')->nullable();

            // Diagnósticos
            $table->string('tipo_diagnostico')->nullable();
            $table->integer('diagnostico_abuelo_id')->nullable()->unsigned();
            $table->integer('diagnostico_padre_id')->nullable()->unsigned();
            $table->integer('diagnostico_hijo_id')->nullable()->unsigned();
            //
            $table->text('evolucion')->nullable();
            $table->integer('elemento_id')->nullable()->unsigned()->index();
            $table->integer('derivacion_id')->nullable()->unsigned()->index();

            $table->string('ausentismo')->nullable();
            $table->string('dias_ausentismo')->nullable();
            $table->integer('tipo_intervencion_id')->nullable()->unsigned()->index();
            $table->text('informeinicial')->nullable();
            $table->integer('n_carpeta')->nullable();

            $table->string('detalle_accion')->nullable();
            $table->date('fecha_accion')->nullable();

            $table->integer('estado_id')->nullable()->unsigned()->index();
            $table->integer('empleado_id')->unsigned()->index();

            $table->integer('n_siniestro')->nullable();
            $table->string('adjunto')->nullable();
            $table->integer('prestador_id')->nullable()->unsigned()->index();
            $table->string('interconsulta')->nullable();
            //
            $table->integer('centromedico_id')->nullable()->unsigned()->index();
            $table->integer('especialidadservicio_id')->nullable()->unsigned()->index();
            $table->dateTime('horajornadaaccidente')->nullable();
            $table->dateTime('inicioinasistencialaboral')->nullable();
            $table->boolean('tareahabitualalaccidente')->nullable();

            $table->string('calleocurrenciaaccidente')->nullable();
            $table->integer('agentematerial_id')->nullable()->unsigned()->index();
            $table->string('tiempoexposicionagente')->nullable();
            $table->integer('formadiagnostico_id')->nullable()->unsigned()->index();
            $table->string('solicitante')->nullable();

            $table->string('familiar')->nullable();

            // Info Adminstrativo
            $table->string('lugar_accidente')->nullable();
            $table->time('hora_accidente')->nullable();
            $table->enum('tarea_habitual', ['si', 'no'])->nullable();
            $table->string('calle_accidente')->nullable();
            $table->string('tiempo_exposicion')->nullable();
            $table->integer('forma_accidente_id')->nullable()->unsigned()->index();

            // info examen medico
            $table->string('tipo_examen_medico')->nullable();
            $table->text('detalle_examen_medico')->nullable();


            $table->timestamps();
        });



        Schema::create('eventos_archivo', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('evento_id')->unsigned()->index();
            $table->string('archivo');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
