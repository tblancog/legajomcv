<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntecedentesMedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antecedentes_medicos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombre');

          $table->softDeletes();
          $table->timestamps();
        });

        Schema::create('antecedentes_grupo_riesgo', function (Blueprint $table) {
          $table->integer('antecedente_id')->unsigned();
          $table->integer('grupo_riesgo_id')->unsigned();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
