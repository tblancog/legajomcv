<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name', 45);
          $table->string('description', 45)->nullable();
          $table->timestamps();
        });

        Schema::create('role_permissions', function (Blueprint $table) {
          $table->integer('role_id')->unsigned();
          $table->integer('permission_id')->unsigned();
          $table->boolean('is_readonly');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
