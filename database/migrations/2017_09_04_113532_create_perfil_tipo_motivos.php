<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilTipoMotivos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil_tipo_motivos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('perfil_id')->unsigned()->index();
            $table->integer('tipo_evento_id')->unsigned()->index();
            $table->integer('motivo_id')->nullable()->unsigned()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
