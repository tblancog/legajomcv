@extends('layouts.app')

@section('content')
  @component('components.show')
    @slot('header')
      <h2>Detalles de Empleado: {{ $item->userad }}</h2>
    @endslot

    @slot('fields')

      <div class="col-md-6">
        <h3>Nombre</h3>
        <p>{{ $item->nombre }}</p>
      </div>
      <div class="col-md-6">
        <h3>Apellido</h3>
        <p>{{ $item->apellido }}</p>
      </div>
      <div class="col-md-6">
        <h3>Sector</h3>
        <p>{{ $item->sector }}</p>
      </div>
      <div class="col-md-6">
        <h3>Documento</h3>
        <p>{{ $item->documento }}</p>
      </div>
      <div class="col-md-6">
        <h3>Cuil</h3>
        <p>{{ $item->cuil }}</p>
      </div>
      <div class="col-md-6">
        <h3>Legajo</h3>
        <p>{{ $item->legajo }}</p>
      </div>
      <div class="col-md-6">
        <h3>Puesto</h3>
        <p>{{ $item->puesto }}</p>
      </div>
      <div class="col-md-6">
        <h3>Gerencia</h3>
        <p>{{ $item->gerencia }}</p>
      </div>
      <div class="col-md-6">
        <h3>Creado</h3>
        <p>{{ $item->created_at->toFormattedDateString() }}</p>
      </div>
      <div class="col-md-6">
        <h3>Actualizado</h3>
        <p>{{ $item->updated_at->toFormattedDateString() }}</p>
      </div>
    @endslot

    @slot('link')
      <div class="row">
        <p><a href="{{ route('users.index') }}" class="btn btn-default">Volver</a></p>
      </div>
    @endslot

  @endcomponent
  <div class="col-md-8 col-md-offset-1">
    <div class="col-md-6">
      <h3>Nombre</h3>
      <p>{{ $item->nombre }}</p>
    </div>
    <div class="col-md-6">
      <h3>Apellido</h3>
      <p>{{ $item->apellido }}</p>
    </div>
    <div class="col-md-6">
      <h3>Sector</h3>
      <p>{{ $item->sector }}</p>
    </div>
    <div class="col-md-6">
      <h3>Documento</h3>
      <p>{{ $item->documento }}</p>
    </div>
    <div class="col-md-6">
      <h3>Cuil</h3>
      <p>{{ $item->cuil }}</p>
    </div>
    <div class="col-md-6">
      <h3>Legajo</h3>
      <p>{{ $item->legajo }}</p>
    </div>
    <div class="col-md-6">
      <h3>Puesto</h3>
      <p>{{ $item->puesto }}</p>
    </div>
    <div class="col-md-6">
      <h3>Gerencia</h3>
      <p>{{ $item->gerencia }}</p>
    </div>
    <hr/>
    <div class="row">
      {{-- <p><a href="{{ route('users.index') }}" class="btn btn-default">Volver</a></p> --}}
    </div>
  </div>
@endsection
