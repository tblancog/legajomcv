@extends('layouts.app')
@section('content')
@component('users.form', ['title' => 'Crear Usuario',
                                        'form_options' => ['url' => ['users.store'],
                                                           'method' => 'POST' ] ,
                                        'model'=> new \App\User() ] )

@endcomponent
@endsection
