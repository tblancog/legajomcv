<div class="col-md-4 col-md-offset-1">

  <h2>{{ $title }}</h2>
  <div class="form-area">
    {!! Form::model($model, ['route'=> $form_options['url'],
                              'method'=> $form_options['method'],
                              'novalidate'=> 'novalidate'])
                            !!}

      {{ csrf_field() }}

      <br style="clear:both">

      {{-- Fields --}}
      <div class="form-group">
        {{ Form::label('userad', 'Usuario Ad') }}
        {{ Form::text('userad', null,
                                ['placeholder'=> 'Usuario AD',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('nombre', 'Nombre') }}
        {{ Form::text('nombre', null,
                                ['placeholder'=> 'Nombre',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('apellido', 'Apellido') }}
        {{ Form::text('apellido', null,
                                ['placeholder'=> 'Apellido',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('sector', 'Sector') }}
        {{ Form::text('sector', null,
                                ['placeholder'=> 'Sector',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('documento', 'Documento') }}
        {{ Form::text('documento', null,
                                ['placeholder'=> 'Documento',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('cuil', 'Cuil') }}
        {{ Form::text('cuil', null,
                                ['placeholder'=> 'Cuil',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('puesto', 'Puesto') }}
        {{ Form::text('puesto', null,
                                ['placeholder'=> 'Puesto',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('legajo', 'Legajo') }}
        {{ Form::text('legajo', null,
                                ['placeholder'=> 'Legajo',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('gerencia', 'Gerencia') }}
        {{ Form::text('gerencia', null,
                                ['placeholder'=> 'Gerencia',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('estado', 'Estado') }}
        <div class="radio">
          <label>
            Activo ?
            {{ Form::hidden('estado',0  ) }}
            {{ Form::checkbox('estado') }}
          </label>
        </div>
      </div>



      <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('users.index') }}" class="btn btn-default">Volver</a>
      </div>

      @include('partials.form.errors')

    {!! Form::close() !!}

  </div>
</div>
