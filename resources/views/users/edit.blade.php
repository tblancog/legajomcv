@extends('layouts.app')
@section('content')
@component('users.form', ['title' => 'Modificar Usuario',
                                        'form_options' => ['url' => ['users.update', 'id'=> $item->id],
                                                           'method' => 'PATCH' ] ,
                                        'model'=> $item ] )

@endcomponent
@endsection
