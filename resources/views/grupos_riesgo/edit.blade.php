@extends('layouts.app')
@section('content')
@component('grupos_riesgo.form', ['title' => 'Modificar Grupo de Riesgo',
                                  'form_options' => ['url' => ['grupos_riesgo.update', 'id'=> $item->id],
                                                     'method' => 'PATCH' ] ,
                                  'model'=> $item,
                                  'selected' => $item->antecedentes->pluck(['id']),
                                  'all' => \App\AntecedenteMedico::get(['id', 'nombre AS text'])->toJson()
                                   ] )

@endcomponent
@endsection
