@extends('layouts.app')
@section('content')
@component('grupos_riesgo.form', ['title' => 'Crear Grupo de Riesgo',
                                  'form_options' => ['url' => ['grupos_riesgo.store'],
                                                     'method' => 'POST' ] ,
                                  'model'=> new \App\GrupoRiesgo(),
                                  'selected' => '[]',
                                  'all' => \App\AntecedenteMedico::get(['id', 'nombre AS text'])->toJson()
                                   ])

@endcomponent
@endsection
