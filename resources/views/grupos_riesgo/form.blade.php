@section('styles')
  <link href="{{ asset('css/select2/select2.css') }}" rel="stylesheet">
@endsection

<div class="col-md-4 col-md-offset-1">

  <h2>{{ $title }}</h2>
  <div class="form-area">
    {!! Form::model($model, ['route'=> $form_options['url'],
                              'method'=> $form_options['method'],
                              'novalidate'=> 'novalidate'])
                            !!}

      {{ csrf_field() }}

      <br style="clear:both">

      {{-- Fields --}}
      <div class="form-group">
        <label for="nombre">Nomrbe</label>
        {{ Form::text('nombre', null,
                                ['placeholder'=> 'Nombre',
                                 'class'=> 'form-control']) }}
      </div>
      <div class="form-group">
        <label for="antecedentes">Antecedentes</label>
        <select name="antecedentes[]" multiple class="select2" style= 'width: 100%'></select>
      </div>

      <div class="form-group">
        <label>Estado</label>
        <div class="radio">
          <label>
            Activo ?
            {{ Form::hidden('estado',0  ) }}
            {{ Form::checkbox('estado') }}
          </label>
        </div>
      </div>
      <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('grupos_riesgo.index') }}" class="btn btn-default">Volver</a>
      </div>

      @include('partials.form.errors')

    {!! Form::close() !!}

  </div>
</div>
@push('scripts')
  <script src="{{ asset('js/select2/select2.js') }}"></script>
  <script>
    window.jQuery(document).ready(function () {
      window.jQuery(".select2")
          .select2({
              data: {!! $all !!},
              placeholder: 'Seleccione antecedentes',
              allowClear: true
            })
          .val({!! $selected !!})
          .trigger("change");
      });
  </script>
@endpush
