@extends('layouts.app')

@section('content')

  @component('partials.form.results', compact('items'))

    @slot('title')
      Grupos de Riesgo
    @endslot

    @slot('create_button')
      <a href="{{ route('grupos_riesgo.create') }}" class="btn btn-primary">Crear Nuevo</a>
    @endslot

    @slot('fields')
      <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th class="hidden-xs">Estado</th>
        <th><em class="fa fa-cog"></em></th>
      </tr>
    @endslot

    @slot('list')
      @forelse ($items as $key => $item)
        <tr>
          <td>{{ $item->id }}</td>
          <td>{{ $item->nombre }}</td>
          <td class="hidden-xs">{{ $item->estado ? 'activo' : 'inactivo' }}</td>
          <td align="center">
            <a href="{{ route('grupos_riesgo.edit', ['id'=> $item->id]) }}" title="editar" class="btn btn-default"><em class="fa fa-pencil"></em></a>
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="4">No hay registros</td>
        </tr>
      @endforelse
    @endslot

  @endcomponent
@endsection
