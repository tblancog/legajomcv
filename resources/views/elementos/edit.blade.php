@extends('layouts.app')
@section('content')
@component('elementos.form', ['title' => 'Modificar Elemento Médico',
                                        'form_options' => ['url' => ['elementos.update', 'id'=> $item->id],
                                                           'method' => 'PATCH' ] ,
                                        'model'=> $item ] )

@endcomponent
@endsection
