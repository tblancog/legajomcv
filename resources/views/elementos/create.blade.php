@extends('layouts.app')
@section('content')
@component('elementos.form', ['title' => 'Crear Elemento Médico',
                                        'form_options' => ['url' => ['elementos.store'],
                                                           'method' => 'POST' ] ,
                                        'model'=> new \App\Elemento() ] )

@endcomponent
@endsection
