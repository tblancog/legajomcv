@extends('layouts.app')
@section('content')
@component('empleados.form', ['title' => 'Modificar Empleados',
                                        'form_options' => ['url' => ['empleados.update', 'id'=> $item->id],
                                                           'method' => 'PATCH' ] ,
                                        'model'=> $item ] )

@endcomponent
@endsection
