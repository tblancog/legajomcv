<div class="col-md-4 col-md-offset-1">

  <h2>{{ $title }}</h2>
  <div class="form-area">
    {!! Form::model($model, ['route'=> $form_options['url'],
                              'method'=> $form_options['method'],
                              'novalidate'=> 'novalidate'])
                            !!}

      {{ csrf_field() }}

      <br style="clear:both">

      {{-- Fields --}}

      <div class="form-group">
        {{ Form::label('foto', 'Foto') }}
        <img src="{{ Faker\Factory::create()->imageUrl }}" alt="">
        {{ Form::file('foto', null,
                                ['placeholder'=> 'Foto',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::text('nombre', null,
                                ['placeholder'=> 'Nombre',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('apellido', 'Apellido') }}
        {{ Form::text('apellido', null,
                                ['placeholder'=> 'Apellido',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('dni', 'Apellido') }}
        {{ Form::text('dni', null,
                                ['placeholder'=> 'Dni',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('genero', 'Cuil') }}
        {{ Form::text('cuil', null,
                                ['placeholder'=> 'Cuil',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('genero', 'Genero') }}
        {{ Form::text('genero', null,
                                ['placeholder'=> 'Genero',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('nacionalidad', 'Nacionalidad') }}
        {{ Form::text('nacionalidad', null,
                                ['placeholder'=> 'Nacionalidad',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('estadocivil', 'Estado Civil') }}
        {{ Form::text('estadocivil', null,
                                ['placeholder'=> 'Estado Civil',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('direccion_personal', 'Dirección Personal') }}
        {{ Form::text('direccion_personal', null,
                                ['placeholder'=> 'Dirección Personal',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('fecha_ingreso', 'Fecha Ingreso') }}
        {{ Form::text('fecha_ingreso', null,
                                ['placeholder'=> 'Fecha Ingreso',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('puesto', 'Gerencia Región Subregión') }}
        {{ Form::text('puesto', null,
                                ['placeholder'=> 'Gerencia Región Subregión',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('perfil', 'Perfil') }}
        {{ Form::text('perfil', null,
                                ['placeholder'=> 'Perfil',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('jefe_directo', 'Jefe Directo') }}
        {{ Form::text('jefe_directo', null,
                                ['placeholder'=> 'Jefe Directo',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('referente_rrhh', 'Referente RRHH') }}
        {{ Form::text('referente_rrhh', null,
                                ['placeholder'=> 'Referente RRHH',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('encuadre', 'Encuadre') }}
        {{ Form::text('encuadre', null,
                                ['placeholder'=> 'Encuadre',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('carga_familia', 'Carga Familia') }}
        {{ Form::text('carga_familia', null,
                                ['placeholder'=> 'Carga Familia',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('art', 'Art') }}
        {{ Form::text('art', null,
                                ['placeholder'=> 'Art',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('obra_social_prepaga', 'Obra Social Prepaga') }}
        {{ Form::text('obra_social_prepaga', null,
                                ['placeholder'=> 'Obra Social Prepaga',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('empresa', 'Empresa') }}
        {{ Form::text('empresa', null,
                                ['placeholder'=> 'Empresa',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        {{ Form::label('domicilio_laboral', 'Domicilio Laboral') }}
        {{ Form::text('domicilio_laboral', null,
                                ['placeholder'=> 'Domicilio Laboral',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        <label>Estado</label>
        <div class="radio">
          <label>
            Activo ?
            {{ Form::hidden('estado',0  ) }}
            {{ Form::checkbox('estado') }}
          </label>
        </div>
      </div>



      <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('empleados.index') }}" class="btn btn-default">Volver</a>
      </div>

      @include('partials.form.errors')

    {!! Form::close() !!}

  </div>
</div>
