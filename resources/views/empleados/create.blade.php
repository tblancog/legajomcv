@extends('layouts.app')
@section('content')
@component('empleados.form', ['title' => 'Crear Empleado',
                                        'form_options' => ['url' => ['empleados.store'],
                                                           'method' => 'POST' ] ,
                                        'model'=> new \App\Elemento() ] )

@endcomponent
@endsection
