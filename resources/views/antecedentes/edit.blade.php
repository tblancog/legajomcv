@extends('layouts.app')
@section('content')
@component('antecedentes.form', ['title' => 'Modificar Antececente Médico',
                                        'form_options' => ['url' => ['antecedentes.update', 'id'=> $item->id],
                                                           'method' => 'PATCH' ] ,
                                        'model'=> $item ] )

@endcomponent
@endsection
