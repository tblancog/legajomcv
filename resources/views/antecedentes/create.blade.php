@extends('layouts.app')
@section('content')
@component('antecedentes.form', ['title' => 'Crear Antececente Médico',
                                        'form_options' => ['url' => ['antecedentes.store'],
                                                           'method' => 'POST' ] ,
                                        'model'=> new \App\AntecedenteMedico() ] )

@endcomponent
@endsection
