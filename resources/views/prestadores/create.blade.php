@extends('layouts.app')
@section('content')
@component('prestadores.form', ['title' => 'Crear Prestador',
                                        'form_options' => ['url' => ['prestadores.store'],
                                                           'method' => 'POST' ] ,
                                        'model'=> new \App\Prestador() ] )

@endcomponent
@endsection
