@extends('layouts.app')
@section('content')
@component('prestadores.form', ['title' => 'Modificar Prestador',
                                        'form_options' => ['url' => ['prestadores.update', 'id'=> $item->id],
                                                           'method' => 'PATCH' ] ,
                                        'model'=> $item ] )

@endcomponent
@endsection
