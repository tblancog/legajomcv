<div class="col-md-4 col-md-offset-1">

  <h2>{{ $title }}</h2>
  <div class="form-area">
    {!! Form::model($model, ['route'=> $form_options['url'],
                              'method'=> $form_options['method'],
                              'novalidate'=> 'novalidate'])
                            !!}

      {{ csrf_field() }}

      <br style="clear:both">

      {{-- Fields --}}
      <div class="form-group">
        {{ Form::text('descripcion', null,
                                ['placeholder'=> 'Descripción',
                                 'class'=> 'form-control']) }}
      </div>

      <div class="form-group">
        <label>Estado</label>
        <div class="radio">
          <label>
            Activo ?
            {{ Form::hidden('estado',0  ) }}
            {{ Form::checkbox('estado') }}
          </label>
        </div>
      </div>



      <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary">Guardar</button>
        <a href="{{ route('tipo_intervencion.index') }}" class="btn btn-default">Volver</a>
      </div>

      @include('partials.form.errors')

    {!! Form::close() !!}

  </div>
</div>
