@extends('layouts.app')
@section('content')
@component('tipo_intervencion.form', ['title' => 'Crear Tipo de Intervención',
                                        'form_options' => ['url' => ['tipo_intervencion.store'],
                                                           'method' => 'POST' ] ,
                                        'model'=> new \App\TipoIntervencion() ] )

@endcomponent
@endsection
