@extends('layouts.app')
@section('content')
@component('tipo_intervencion.form', ['title' => 'Modificar Tipo de Intervención',
                                        'form_options' => ['url' => ['tipo_intervencion.update', 'id'=> $item->id],
                                                           'method' => 'PATCH' ] ,
                                        'model'=> $item ] )

@endcomponent
@endsection
