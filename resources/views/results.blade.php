<div class="col-md-10 col-md-offset-1">
  <div class="panel panel-default panel-table">
    <div class="panel-heading">
      <div class="row">
        <div class="col col-xs-6">
          <h3 class="panel-title">{{ $title }}</h3>
        </div>
        <div class="col col-xs-6 text-right">

          {{-- Slor for create button --}}
          {{ $create_button }}

        </div>
      </div>
    </div>
    <div class="panel-body">
      <table class="table table-striped table-bordered table-list">
        <thead>

          {{-- Slot for fields --}}
          {{ $fields }}

        </thead>
        <tbody>

          {{-- Slot for records --}}
          {{ $list }}

        </tbody>
      </table>

    </div>
    <div class="panel-footer">
        {{ $items->links() }}
    </div>
  </div>

</div>
</div>
</div>
