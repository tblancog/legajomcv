@extends('layouts.app')
@section('content')
@component('motivos.form', ['title' => 'Modificar Motivo Médico',
                                        'form_options' => ['url' => ['motivos.update', 'id'=> $item->id],
                                                           'method' => 'PATCH' ] ,
                                        'model'=> $item ] )

@endcomponent
@endsection
