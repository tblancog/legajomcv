@extends('layouts.app')
@section('content')
@component('motivos.form', ['title' => 'Crear Motivo Médico',
                                        'form_options' => ['url' => ['motivos.store'],
                                                           'method' => 'POST' ] ,
                                        'model'=> new \App\Motivo() ] )

@endcomponent
@endsection
