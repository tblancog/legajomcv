<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DatoMedicoVacuna extends Model
{
    protected $table = 'datos_medicos_vacunas';
    protected $dates = ['created_at','updated_at'];
    public $timestamps = true;

    public function empleado(){
      return $this->belongsTo(\App\Empleado::class);
    }

    public function vacuna() {
    	return $this->belongsTo(\App\Vacuna::class);
    }
}
