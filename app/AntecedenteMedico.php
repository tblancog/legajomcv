<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\GrupoRiesgo;

class AntecedenteMedico extends Model
{
    use SoftDeletes;

    protected $table = 'antecedentes_medicos';
    protected $fillable = ['nombre', 'deleted_at', 'grupo_riesgo_id'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getEstadoAttribute(){
      return $this->deleted_at ? false : true;
    }

    public function scopeBasicAbm ($query){
      return $query->select('id', 'nombre', 'created_at', 'deleted_at')
                  ->orderBy('created_at', 'desc')
                  ->orderBy('updated_at', 'desc')
                  ->withTrashed();
    }

    public function grupos()
    {
      return $this->belongsToMany(\App\GrupoRiesgo::class, 'antecedentes_grupo_riesgo', 'antecedente_id', 'grupo_riesgo_id');
    }
}
