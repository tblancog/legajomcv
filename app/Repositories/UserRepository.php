<?php
namespace App\Repositories;
use App\User;
use App\Permission;
use App\Role;
use Carbon\Carbon;

class UserRepository {

  private $user;
  public function createUser($data){

    $data['deleted_at'] = $data['estado'] ? null : Carbon::now();
    $this->user= User::create( $data );

    // Permissions
    $write = isset($data['write_permissions']) ? $data['write_permissions'] : [];
    $read = isset($data['read_permissions']) ? $data['read_permissions'] : [];

    $this->resetPermissions();
    $this->setPermissionsForUser('write', $write);
    $this->setPermissionsForUser('read', $read);

    return $this->user;
  }

  public function updateUser($data, $entity){

    $data['deleted_at'] = $data['estado'] == '1' ? null : Carbon::now();
    $entity->update( $data );
    $this->user= $entity;

    // Permissions
    $write = isset($data['write_permissions']) ? $data['write_permissions'] : [];
    $read = isset($data['read_permissions']) ? $data['read_permissions'] : [];

    $this->resetPermissions();
    $this->setPermissionsForUser('write', $write);
    $this->setPermissionsForUser('read', $read);

    return $this->user;
  }

  public function resetPermissions(){

      $this->user->permissions()->detach();
  }

  public function setPermissionsForUser($mode, $permissionArray){

    $permission = Permission::whereIn('id', $permissionArray)->get();
    if(count($permission)){
      $permission->each(function( $p ) use ($mode){
        $this->user
        ->permissions()
        ->save( $p, ['is_readonly'=> ($mode == 'read') ? true : false ] );
      });
    }
  }

  public function suggestUsers($term, $max){
     return $this->searchAdUser($term)
                ->select('sAMAccountName as id',
                         'displayName as text',
                         'title as sector')
                 ->take($max)
                 ->get();
  }

  public function findUser($term){
    return [
      'local' => User::searchLocalUser($term)->first(),
      'ad' => $this->findAdUser($term)->first()
    ];
  }

  public function findAdUser($term){
    return $this->searchAdUser($term)
                ->select('sAMAccountName as userad',
                          'displayName as fullname',
                          'givenname as nombre',
                          'sn as apellido',
                          'mail as email',
                          'title as sector',
                          'employeeid as documento',
                          'extensionattribute4 as cuil',
                          'extensionattribute2 as legajo',
                          'department as gerencia');
  }

  public function searchAdUser($q){
    return \DB::connection('ad')
                ->table('personal_db.ad_users_cv')
                ->where('sAMAccountName', $q)
                ->orWhere('mail', $q)
                ->orWhere('displayName', 'like', '%'.$q.'%')
                ->orWhere('givenname', 'like', '%'.$q.'%')
                ->orWhere('sn', 'like', '%'.$q.'%')
                ->orWhere('extensionattribute2', $q)
                ->orWhere('employeeid', $q)
                ->orWhere('extensionattribute4', $q);
  }
}
