<?php
namespace App\Repositories;
use App\Evento;
use Carbon\Carbon;

class DiagnosticoRepository {

  public function getAbuelos($tipo = '', $term = ''){
      return $this->getQueryBuilder('abuelo_diagnosticos', $term)
                ->where(function($query) use ($tipo) {
                  if($tipo !=  null) {
                    $query->where('tipo', $tipo);
                  }})
                ->get();
  }

  public function getPadres($id= null, $term = ''){
      return $this->getQueryBuilder('padre_diagnosticos', $term)
                ->addSelect('abuelo_id')
                ->where(function($query) use ($id){
                  if($id !=  null) {
                    $query->where('abuelo_id', $id);
                  }})
                ->get();
  }

  public function getHijos($id= null, $term = ''){
      return $this->getQueryBuilder('hijo_diagnosticos', $term)
                ->addSelect('padre_id')
                ->where(function($query) use ($id){
                  if($id !=  null) {
                    $query->where('padre_id', $id);
                  }})
                ->get();
  }

  public function getQueryBuilder($table, $term) {
    return \DB::table($table)
                ->select('id')
                ->addSelect(\DB::raw('CONCAT(codigo, " ", descripcion) as text'))
                ->orWhere(function($query) use ($term) {
                  $query->where( 'codigo', 'like', '%'.strtoupper($term).'%' )
                         ->orWhere( 'descripcion', 'like', '%'.strtoupper($term).'%' );
                })
                ->limit(50)
                ->orderBy('codigo');
  }
}
