<?php

namespace App\Repositories;


class LegajoRepository {
  


  private static function buscarUsuarioAd($term, $max) {

    return  \DB::connection('ad')
      ->table('personal_db.ad_users_cv') 
      ->select(
               'employeeid AS documento',
               'givenname AS nombre',
               'sn AS apellido')
      ->where('sAMAccountName', $term) // userAd
      ->orWhere('mail', $term) 
      ->orWhere('givenname', 'like', ''.$term.'%') // nombre
      ->orWhere('sn', 'like', ''.$term.'%')
      ->orWhere('extensionattribute2', $term) // legajo
      ->orWhere('employeeid', $term) // numero de documento
      ->orWhere('extensionattribute4', $term) // cuit/cuil
      ->orderBy("sn","ASC")
      ->orderBy("givenname","ASC")
      ->take($max)
      ->get();

  }



  private static function buscarUsuarioSap($term, $max) {

    return  \DB::connection('ad')
      ->table('personal_db.sap_users_cv') 
      ->select(
               'num_documento AS documento',
               'nombre',
               'primer_apellido AS apellido')
      ->where('usr_ad', $term) // userAd
      ->orWhere('mail_laboral', $term) 
      ->orWhere('nombre', 'like', ''.$term.'%') // nombre
      ->orWhere('primer_apellido', 'like', ''.$term.'%') // apellido
      ->orWhere('Legajo', $term) // legajo
      ->orWhere('num_documento', $term) // numero de documento
      ->orWhere('num_cuil', $term) // cuit/cuil
      ->orderBy("primer_apellido","ASC")
      ->orderBy("nombre","ASC")
      ->take($max)
      ->get();
  }



  private static function buscarUsuarioLocal($term, $max) {
    return \App\Empleado::select('dni as documento',
                                 'nombre',
                                 'apellido')
          ->where('dni', $term) // numero de documento
          ->orWhere('nombre', 'like', ''.$term.'%') // nombre
          ->orWhere('apellido', 'like', ''.$term.'%') // apellido
          ->orWhere('legajo', $term) // legajo
          ->orWhere('cuil', $term) // cuit/cuil
          ->orderBy("apellido","ASC")
          ->orderBy("nombre","ASC")
          ->take($max)
          ->get();
    
  }



  private static function seleccionarUsuarioSap($documento, $apellido=false) {

    return  \DB::connection('ad')
      ->table('personal_db.sap_users_cv') 
      ->where('num_documento', $documento) // numero de documento
      ->get();
  }



  private static function seleccionarUsuarioAd($documento, $apellido=false) {

    return  \DB::connection('ad')
          ->table('personal_db.ad_users_cv') 
          ->where('employeeid', $documento) // numero de documento
          ->get();
  }


  private static function seleccionarUsuarioLocal($documento, $apellido=false) {

    return  \App\Empleado::where("dni",$documento)->get();
  }




  public static function buscarLegajos($term, $max=5) {

    $collectSap = self::buscarUsuarioSap($term,$max);
    $collectAd = self::buscarUsuarioAd($term,$max);
    $collectLocal = self::buscarUsuarioLocal($term,$max);
    $merge =  $collectSap->merge($collectAd)->merge($collectLocal);

    if ($merge->count() == 0) {
      return [];
    }
    
    $uniques = $merge->unique(function($item) {
      return $item->documento . strtoupper($item->nombre) . strtoupper($item->apellido);
    });


    $sort = $uniques->sortBy(function($item, $key) {
        return strtoupper($item->apellido . $item->nombre);
    });

    
    $splice = $sort->splice($max);

    return $sort->all();
  }



  public static function buscarVacunas($term, $max) {

    return \App\Vacuna::select('id','nombre')
          ->where('nombre', 'like', '%'. $term . '%') 
          ->withTrashed()         
          ->orderBy("nombre","ASC")
          ->take($max)
          ->get();
  }



   public static function buscarAntecedentes($term, $max) {

    return \App\AntecedenteMedico::select('id','nombre')
          ->where('nombre', 'like', '%'. $term . '%') 
          ->withTrashed()         
          ->orderBy("nombre","ASC")
          ->take($max)
          ->get();
  }




  // selecciona el documento y en caso de no existir lo sincroniza localmente con SAP y AD segun corresponda
  public static function seleccionar($documento, $apellido=false) {

 
  
    $collectSap = self::seleccionarUsuarioSap($documento, $apellido);

    $collectAd = self::seleccionarUsuarioAd($documento,$apellido);

    $collectLocal = self::seleccionarUsuarioLocal($documento,$apellido); 

    // caso que no exista en ninguna de las tres alternativas

    if ($collectSap->count()==0 and $collectAd->count()==0 and $collectLocal->count()==0) {

      return false;
    }


    // caso que existe localmente

    if ($collectSap->count()==0 and $collectAd->count()==0) {

      return $collectLocal->first();
    }


    

    
    $tipo = "EXTERNO";

    if ($collectSap->count() > 0) {

      $tipo = "NOMINA";
    }

    // obtengo los datos de SAP y de AD

    $empleadoSap = $collectSap->count()==0?false:$collectSap->first();

    $empleadoAd = $collectAd->count()==0?false:$collectAd->first();

   

    // populo los datos del legajo tomando como prioridad SAP
    $data = [];
    
    $data['nombre'] = $empleadoSap?$empleadoSap->nombre:$empleadoAd->givenname;
    
    $data['apellido'] = $empleadoSap?$empleadoSap->primer_apellido:$empleadoAd->sn;
    
    $data['dni'] = $documento;
    
    $data['legajo'] = $empleadoSap?$empleadoSap->Legajo:$empleadoAd->extensionattribute2; 
    
    $data['cuil'] = $empleadoSap?$empleadoSap->num_cuil:$empleadoAd->extensionattribute4; 
    
    $data['genero'] = $empleadoSap?$empleadoSap->des_sexo:null;
    
    $data['nacionalidad'] = $empleadoSap?$empleadoSap->des_nacionalidad:null;


    $data['estadocivil'] = $empleadoSap?$empleadoSap->des_estado_civil:null;

    //$data['fecha_ingreso'] = $empleadoSap?$empleadoSap->fech_ingreso:null;
    
    //$data['fecha_nacimiento'] = $empleadoSap?(substr(0,4,$empleadoSap->fech_nacimiento) . "-" . substr(3,2,$empleadoSap->fech_nacimiento) . "-" . substr(5,2,$empleadoSap->fech_nacimiento) . "-" ):null;

    $data['puesto'] = $empleadoSap?$empleadoSap->des_posicion:null;

    $data['codigo_puesto'] = $empleadoSap?$empleadoSap->cod_posicion:null;

    $data['gerencia'] = $empleadoSap?$empleadoSap->des_area_funcional:$empleadoAd->department;

    $data['region'] = $empleadoSap?$empleadoSap->des_region:$empleadoAd->l;

    $data['subregion'] = $empleadoSap?$empleadoSap->des_subdiv_personal:null;

    $data['perfil'] = $empleadoSap?$empleadoSap->des_funcion:null;

    $data['jefe_directo'] = $empleadoSap?$empleadoSap->nombre_completo_jefe:null;
    
    $data['referente_rrhh'] = $empleadoSap?$empleadoSap->des_adm_nomina:null;

    $data['encuadre'] = $empleadoSap?$empleadoSap->des_area_personal:null;

    $data['obra_social_prepaga'] = $empleadoSap?$empleadoSap->des_osocial_recibo:null;

    $data['empresa'] = $empleadoSap?$empleadoSap->des_empresa:$empleadoAd->company;

    $data['domicilio_laboral'] = $empleadoSap?$empleadoSap->dir_recibo:null;

    $data['telefono_laboral'] = $empleadoSap?$empleadoSap->telefono_laboral:$empleadoAd->otherTelephone;


    $data['domicilio_particular'] ="";

    $data['telefono_particular'] ="";

    if ($empleadoSap) {

      $data['domicilio_particular'] = $empleadoSap->part_calle . " " . $empleadoSap->part_altura . " " . $empleadoSap->part_piso . " " . $empleadoSap->part_depto . " " . $empleadoSap->part_cod_postal . " " . $empleadoSap->part_localidad . " " . $empleadoSap->part_provincia;

      $data['telefono_particular'] = $empleadoSap->part_telefono;

    }

    $data['tipo'] = $tipo;

    // en caso de no existir localmente el empleado, lo creo
    if ($collectLocal->count() == 0) {

      $legajo = \App\Empleado::create($data);


    } else { // caso contrario, actualizamos los datos del empleado

      $legajo = $collectLocal->first();

      $legajo->update($data);

      $legajo->direcciones()->where("descripcion","principal")->delete();

      $legajo->telefonos()->where("descripcion","principal")->delete();

     
    }


    if (trim($data["domicilio_particular"])!="") {
        
      $direccion = new \App\Direccion();
      $direccion->direccion = $data["domicilio_particular"];
      $direccion->descripcion = "principal";
      $direccion->empleado()->associate($legajo);
      $direccion->save();

    }


    if (trim($data["telefono_particular"])!="") {
        
      $direccion = new \App\Telefono();
      $direccion->numero = $data["telefono_particular"];
      $direccion->descripcion = "principal";
      $direccion->empleado()->associate($legajo);
      $direccion->save();

    }

    return $legajo;

  }


  public static function direccionPrincipal(\App\Empleado $empleado) {

    return $empleado->direcciones()
               ->select('direccion')
               ->where('descripcion','principal')
               ->first();
  }



  public static function telefonoPrincipal(\App\Empleado $empleado) {

    return $empleado->telefonos()
               ->select('numero')
               ->where('descripcion','principal')
               ->first();
  }



  public static function direccionesAlternativas(\App\Empleado $empleado) {

    return $empleado->direcciones()
               ->select('direccion')
               ->where('descripcion','alternativa')
               ->orderBy('id','desc')
               ->get();
  }



  public static function telefonosAlternativos(\App\Empleado $empleado) {
    return $empleado->telefonos()
                    ->select('numero')
                    ->where('descripcion','alternativo')
                    ->orderBy('id','desc')
                    ->get();
  }


  public static function vacunasAplicadas(\App\Empleado $empleado) {
    return $empleado->vacunas()->select("vacunas.id","vacunas.nombre")->get();
  }


  public static function antecedentes(\App\Empleado $empleado) {
    return $empleado->antecedentes()->select("antecedentes_medicos.id","antecedentes_medicos.nombre")->get();
  }


  public static function historiaMedica(\App\Empleado $empleado) {
    
    $eventos =  $empleado->eventos()->get();

    $result = [];

    foreach ($eventos as $evento) {

      $result [] = ["tipo_evento"=>$evento->tipo()->descripcion,
                    "motivo_consulta"=>$evento->motivo()->nombre   
      ];
    }

    return $result;
  }
 
 
  
}


