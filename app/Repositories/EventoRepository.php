<?php
namespace App\Repositories;
use App\Evento;
use Carbon\Carbon;

class EventoRepository {
  private $event;

  public function listEventos() {
    return Evento::listEventos()
                  ->select('eventos.id as id')
                  ->addSelect('fecha_consulta')
                  ->addSelect(\DB::raw('CONCAT(fecha_consulta, ", ", nombre) as text'));
  }
  public function listEventsByMotivo($motivo = false) {
    return $this->listEventos()
                ->where('motivos.slug', '=' ,$motivo)
                ->get();
  }
  public function listEventsByTipoEvento($tipoevento = false) {
    return $this->listEventos()
                ->where('tipos_evento.slug', '=', $tipoevento)
                ->get();
  }
  public function listEventsByGrupoDerivacion($grupo = false) {
    return $this->listEventos()
                ->addSelect('derivaciones.grupo',
                            'diagnostico_abuelo_id',
                            'diagnostico_padre_id',
                            'diagnostico_hijo_id',
                            'motivo_id' )
                ->addSelect(\DB::raw('CONCAT(abuelo_diagnosticos.codigo, ", ", abuelo_diagnosticos.descripcion) as abuelo'))
                ->addSelect(\DB::raw('CONCAT(padre_diagnosticos.codigo, ", ", padre_diagnosticos.descripcion) as padre'))
                ->addSelect(\DB::raw('CONCAT(hijo_diagnosticos.codigo, ", ", hijo_diagnosticos.descripcion) as hijo'))
                ->where('derivaciones.grupo', '=', $grupo)
                ->join('derivaciones', 'eventos.derivacion_id', '=', 'derivaciones.id')
                // Diagnosticos
                ->join('abuelo_diagnosticos', 'eventos.diagnostico_abuelo_id', '=', 'abuelo_diagnosticos.id')
                ->join('padre_diagnosticos', 'eventos.diagnostico_padre_id', '=', 'padre_diagnosticos.id')
                ->join('hijo_diagnosticos', 'eventos.diagnostico_hijo_id', '=', 'hijo_diagnosticos.id')
                ->orderBy('fecha_consulta', 'desc')
                ->get();
  }
}
