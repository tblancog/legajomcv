<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Elemento extends Model
{
    use SoftDeletes;

    protected $table = 'elementos';
    protected $fillable = ['nombre', 'deleted_at'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getEstadoAttribute(){
      return $this->deleted_at ? false : true;
    }

    public function scopeBasicAbm ($query){
      return $query->select('id', 'nombre', 'created_at', 'deleted_at')
                  ->orderBy('created_at', 'desc')
                  ->orderBy('updated_at', 'desc')
                  ->withTrashed();
    }
}
