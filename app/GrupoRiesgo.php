<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use App\GrupoRiesgo;
use App\AntecedenteMedico;

class GrupoRiesgo extends Model
{
    use SoftDeletes;

    protected $table = 'grupos_riesgo';
    protected $fillable = ['nombre', 'deleted_at'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function getEstadoAttribute(){
      return $this->deleted_at ? false : true;
    }

    public function scopeBasicAbm ($query){
      return $query->select('id', 'nombre', 'created_at', 'deleted_at')
                  ->orderBy('created_at', 'desc')
                  ->orderBy('updated_at', 'desc')
                  ->withTrashed();
    }

    public function antecedentes()
    {
      return $this->belongsToMany(\App\AntecedenteMedico::class, 'antecedentes_grupo_riesgo', 'grupo_riesgo_id', 'antecedente_id');
    }
}
