<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    protected $table = 'telefonos';

    public function empleado(){
        return $this->belongsTo(\App\Empleado::class);
    }
}
