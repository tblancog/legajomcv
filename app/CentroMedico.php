<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentroMedico extends Model
{
    protected $table = 'centros_medicos';
}
