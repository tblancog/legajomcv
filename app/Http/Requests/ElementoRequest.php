<?php

namespace App\Http\Requests;

use \App\Http\Requests\ApiRequest;

class ElementoRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $item= $this->route('elemento');

      $id= ($item) ? $item->id : null;
        return [
            'nombre' => 'required|max:45|string|unique:elementos,nombre,'.$id,
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El elemento es requerido',
            'nombre.unique'  => 'El elemento ya se encuentra registrado',
            'nombre.max' => 'El máximo de caracteres permitidos es de 50',
            'estado.required' => 'El estado del elemento es requerido',
        ];
    }
}
