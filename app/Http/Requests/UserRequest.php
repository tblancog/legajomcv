<?php

namespace App\Http\Requests;

use \App\Http\Requests\ApiRequest;

class UserRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $item= $this->route('user');

      $id= ($item) ? $item->id : null;
        return [
            'userad' => 'max:45|string|unique:users,userad,'.$id,
            'email' => 'required|max:45|email|unique:users,email,'.$id,
            'role_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'El email de ad es requerido',
            'userad.unique'  => 'El usuario de ad ya se encuentra registrado',
            'userad.max' => 'El máximo de caracteres permitidos es de 50',
            'estado.required' => 'El estado del usuario es requerido',
            'role_id.required' => 'Al menos debe tener un rol',
        ];
    }
}
