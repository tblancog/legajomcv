<?php

namespace App\Http\Requests;

use \App\Http\Requests\ApiRequest;

class EmpleadoRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $item= $this->route('empleado');

      $id= ($item) ? $item->id : null;
        return [
            'nombre' => 'required|max:45',
            'apellido' => 'required|max:45',
            'dni' => 'required|max:45',
            'cuil' => 'required|max:45',
            'genero' => 'required|max:45',
            'nacionalidad' => 'required|max:45',
            'estadocivil' => 'required|max:45',
            'direccion_personal' => 'required|max:45',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre del empleado es requerido',
            'nombre.max' => 'El máximo de caracteres permitidos es de 45',

            'apellido.required' => 'El apellido del empleado es requerido',
            'apellido.max' => 'El máximo de caracteres permitidos es de 45',

            'estado.required' => 'El estado del empleado es requerido',
        ];
    }
}
