<?php

namespace App\Http\Requests;

use \App\Http\Requests\ApiRequest;

class GrupoRiesgoRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $item= $this->route('grupo');

      $id= ($item) ? $item->id : null;
        return [
            'nombre' => 'required|max:45|string|unique:grupos_riesgo,nombre,'.$id,
            'antecedentes' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre del grupo de riesgo es requerido',
            'nombre.unique'  => 'El nombre del grupo de riesgo ya se encuentra registrado',
            'nombre.max' => 'El máximo de caracteres permitidos es de 50',
            'estado.required' => 'El estado del grupo de riesgo es requerido',

            'antecedentes.required' => 'Al menos debe existir un antecedente',
        ];
    }
}
