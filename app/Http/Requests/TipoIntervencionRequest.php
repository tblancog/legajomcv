<?php

namespace App\Http\Requests;

use \App\Http\Requests\ApiRequest;

class TipoIntervencionRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $item= $this->route('tipo');

      $id= ($item) ? $item->id : null;
        return [
            'nombre' => 'required|max:45|string|unique:tipos_intervencion,nombre,'.$id,
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El tipo de intervencion es requerido',
            'nombre.unique'  => 'El tipo de intervencion ya se encuentra registrado',
            'nombre.max' => 'El máximo de caracteres permitidos es de 50',
            'estado.required' => 'El estado del tipo de intervencion es requerido',
        ];
    }
}
