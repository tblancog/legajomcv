<?php

namespace App\Http\Requests;
use \App\Http\Requests\ApiRequest;

class AntecedenteMedicoRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      $item= $this->route('antecedente');
      $id= ($item) ? $item->id : null;
      return [
            'nombre' => 'required|max:45|string|unique:antecedentes_medicos,nombre,'.$id,
      ];
    }
}
