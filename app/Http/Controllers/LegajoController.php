<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\LegajoRepository;



class LegajoController extends Controller
{
 
    public function suggestLegajos($term){
    
        $results = LegajoRepository::buscarLegajos($term, 5);

        return response()->json($results);
      
    }



    public function suggestVacunas($term){
    
        $results = LegajoRepository::buscarVacunas($term, 5);

        return response()->json($results);
      
    }


    public function suggestAntecedentes($term){
    
        $results = LegajoRepository::buscarAntecedentes($term, 5);

        return response()->json($results);
      
    }


    

    public function find($documento,$apellido) {

        
        $legajo=LegajoRepository::seleccionar($documento, $apellido);

        $result = ["_existe"=>false];

        if ($legajo !==false) {

          $result= ["_existe"=>true, "legajo"=>$legajo];
        }

        return \Response::json($result,200);
    }

    


    public function datosLegajo(\App\Empleado $empleado) {

      $result = ["datosPersonales"=>$empleado,
                 "datosMedicos"=>$empleado->datosMedicos()->first(),
                 "direccionPrincipal"=>LegajoRepository::direccionPrincipal($empleado),
                 "direccionesAlternativas"=>LegajoRepository::direccionesAlternativas($empleado),
                 "telefonoPrincipal"=>LegajoRepository::telefonoPrincipal($empleado),
                 "telefonosAlternativos"=>LegajoRepository::telefonosAlternativos($empleado),
                 "vacunasAplicadas"=>LegajoRepository::vacunasAplicadas($empleado),
                 "antecedentes"=>LegajoRepository::antecedentes($empleado),
                 "historiaMedica"=>LegajoRepository::historiaMedica($empleado)



      ];
                                           
      return \Response::json($result,200);
    }




    public function direccionesAlternativas(\App\Empleado $empleado) {

      return \Response::json(LegajoRepository::direccionesAlternativas($empleado),200);

    }



    public function telefonosAlternativos(\App\Empleado $empleado) {

      return \Response::json(LegajoRepository::telefonosAlternativos($empleado),200);

    }



    public function vacunasAplicadas(\App\Empleado $empleado) {
                                             
      return \Response::json(LegajoRepository::vacunasAplicadas($empleado),200);

    }



    public function antecedentes(\App\Empleado $empleado) {
                                             
      return \Response::json(LegajoRepository::antecedentes($empleado),200);

    }





    public function saveDireccionAlternativa(\App\Empleado $empleado, Request $request) {

      $direccion = new \App\Direccion();

      $direccion->direccion = $request->input('direccion');

      $direccion->descripcion = "alternativa";

      $direccion->empleado()->associate($empleado);

      $direccion->save();

      return \Response::json(["save"=>true],200);

    }



    public function saveTelefonoAlternativo(\App\Empleado $empleado, Request $request) {

      $telefono = new \App\Telefono();

      $telefono->numero = $request->input('telefono');

      $telefono->descripcion = "alternativo";

      $telefono->empleado()->associate($empleado);

      $telefono->save();

      return \Response::json(["save"=>true],200);

    }


    public function saveVacunasAplicadas(\App\Empleado $empleado, Request $request) {

      $vacunas = $request->input('vacunas');

      $empleado->vacunas()->sync($vacunas);

      return \Response::json(["save"=>true],200);

    }


    public function saveAntecedentes(\App\Empleado $empleado, Request $request) {

      $antecedentes = $request->input('antecedentes');

      $empleado->antecedentes()->sync($antecedentes);

      return \Response::json(["save"=>true],200);

    }


    public function saveDatosMedicos(\App\Empleado $empleado, Request $request) {


      $dm = $empleado->datosMedicos()->first();

      if (!$dm)  {
        $dm = new \App\DatoMedico();
      }

      $dm->dador_sangre = $request->input("dador_sangre");
      $dm->certificado_discapacidad = $request->input("certificado_discapacidad");
      $dm->tipo_discapacidad = implode(";",$request->input("tipo_discapacidad"));
      $dm->grupo_sanguineo = $request->input("grupo_sanguineo");
      $dm->exposicion_riesgo_laboral = $request->input("exposicion_riesgo_laboral");
      $dm->miembro_superhabil = $request->input("miembro_superhabil");
      $dm->donante_organos = $request->input("donante_organos");
      $dm->peso = $request->input("peso_kg") . "." . $request->input("peso_gr");
      $dm->altura = $request->input("altura_mt") . "." . $request->input("altura_cm");

      $dm->empleado()->associate($empleado);

      $dm->save();

      return \Response::json(["save"=>true],200);

    }


    public function uploadExamenMedico($examen, Request $request) {

      return \Response::json(["save"=>true],200);
    }

}
