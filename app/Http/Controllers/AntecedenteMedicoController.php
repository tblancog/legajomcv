<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AntecedenteMedico;
use Carbon\Carbon;
use App\Http\Requests\AntecedenteMedicoRequest;
use Yajra\Datatables\Facades\Datatables;

class AntecedenteMedicoController extends Controller
{

  /**
   * Index view of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      // $items =AntecedenteMedico::allTrashed()
      //                           ->paginate(config('paginate.antecedentes'));
      return \Datatables::collection( AntecedenteMedico::basicAbm()->get())
                          ->make(true);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('antecedentes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (AntecedenteMedicoRequest $request)
    {
      $input = $request->all();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      return AntecedenteMedico::create(
        $input
      );
    }

    /**
     * Display the specified resource.
     * @param  AntecedenteMedico $entity
     * @return \Illuminate\Http\Response
     */
    public function show(AntecedenteMedico $entity)
    {
      return $entity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AntecedenteMedico $item)
    {
        return view('antecedentes.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AntecedenteMedicoRequest $request, AntecedenteMedico $entity)
    {
      $input = $request->all();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      $entity->update($input);
      return $entity;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  AntecedenteMedico $entity
     * @return \Illuminate\Http\Response
     */
    public function destroy(AntecedenteMedico $entity)
    {
      return $entity->delete();
    }
}
