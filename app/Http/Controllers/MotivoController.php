<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Motivo;
use Carbon\Carbon;
use App\Http\Requests\MotivoRequest;
class MotivoController extends Controller
{
  /**
   * Index view of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      // $items =Motivo::allTrashed()
      //                           ->paginate(config('paginate.motivos'));
      // return view('motivos.index', ['items'=> $items]);
      return \Datatables::collection( Motivo::basicAbm()->get())
                          ->make(true);
  }

  public function list( $tipoEvento = null ){
    if(is_null($tipoEvento)){
      return Motivo::get(['id', 'slug as value', 'nombre as text']);
    }
    return  Motivo::where('tipo_evento_id', $tipoEvento)->get();
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('motivos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MotivoRequest $request)
    {
      $input = $request->all();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      return Motivo::create(
        $input
      );
    }

    /**
     * Display the specified resource.
     * @param  Motivo $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Motivo $entity)
    {
      return $entity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Motivo $item)
    {
        return view('motivos.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MotivoRequest $request, Motivo $entity)
    {
       $input = $request->all();
       $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
       $entity->update($input);
       return $entity;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Motivo::find($id)->delete();
    }
}
