<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DiagnosticoRepository;

class DiagnosticoController extends Controller
{
    protected $repo;

    public function __construct(DiagnosticoRepository $repo){
        $this->repo = $repo;
    }
    public function abuelos(){
      return $this->repo->getAbuelos(request()->input('tipo'),
                                     request()->input('q'));
    }  

    public function padres($idAbuelo = null){
      return $this->repo->getPadres(request()->input('id'),
                                    request()->input('q'));
    }

    public function hijos($id = null){
      return $this->repo->getHijos(request()->input('id'),
                                    request()->input('q'));
    }
}
