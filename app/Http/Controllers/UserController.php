<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use App\User;
use Carbon\Carbon;
use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;
class UserController extends Controller
{

  protected $repo;

  public function __construct(UserRepository $userRepo){
      $this->repo = $userRepo;
  }

  /**
   * Index view of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      return \Datatables::collection( User::basicAbm()->get())
                          ->make(true);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
      return $this->repo
                  ->createUser($request->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $item)
    {
        return view('users.edit', compact('item'));
    }

    /**
    * Shows details for specified resource
    *
    * @param User $items
    * @return \Illuminate\Http\Response
    */
    public function show($key)
    {
      return \App\User::findByKey($key)->first();
    }

    public function suggest(Request $request){
      $term = $request->input('term');
      if( !$user = $this->repo->suggestUsers($term, 5) ){
        return response()->json(['status'=> 404,
                                 'message'=> 'No results']);
      }else{
        return response()->json($user);
      }
    }

    public function search($userad){
      $user = $this->repo->findUser($userad);
      if($user['local']){
        $response = 'local';
      }else{
        $response = $user['ad'];
      }
      return response()->json($response);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $entity)
    {
      return $this->repo
                  ->updateUser($request->all(), $entity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
    }
}
