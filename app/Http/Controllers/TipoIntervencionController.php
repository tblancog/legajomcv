<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoIntervencion;
use Carbon\Carbon;
use App\Http\Requests\TipoIntervencionRequest;
class TipoIntervencionController extends Controller
{
  /**
   * Index view of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \Datatables::collection( TipoIntervencion::basicAbm()->get())
                        ->make(true);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tipo_intervencion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TipoIntervencionRequest $request)
    {
      $input = $request->all();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      return TipoIntervencion::create(
        $input
      );
    }

    /**
     * Display the specified resource.
     * @param  TipoIntervencion $entity
     * @return \Illuminate\Http\Response
     */
    public function show(TipoIntervencion $entity)
    {
      return $entity;
    }

    public function list(){
      return \App\TipoIntervencion::get(['id', 'nombre as text']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoIntervencion $item)
    {
        return view('tipo_intervencion.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TipoIntervencionRequest $request, TipoIntervencion $antecedente)
    {

       $input = $request->all();
       $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
       $antecedente->update($input);
       return $antecedente;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TipoIntervencion::find($id)->delete();
    }
}
