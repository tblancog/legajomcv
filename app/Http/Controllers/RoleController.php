<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Role;
use \App\TipoEvento;

class RoleController extends Controller
{
    public function list() {
      return Role::get(['id', 'description as text']);
    }

    public function tipos(Role $role) {
      return $role->tiposEvento()->distinct()->get()->map(function($el) {
                    return collect(['id'=> $el->id,
                                    'text' => $el->descripcion]);
                  })->unique('text');
    }

    public function motivos(Role $role, $tipo_evento_id) {
      return $role->motivosConsulta()
                  ->distinct()
                  ->where("tipo_evento_id",$tipo_evento_id)
                  ->get()
                  ->map(function($el) {
                    return collect(['id'=> $el->id,
                                    'text' => $el->nombre,
                                    'slug' => $el->slug
                                  ]);
                  })->unique('slug');
    }
}
