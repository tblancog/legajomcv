<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Elemento;
use Carbon\Carbon;
use App\Http\Requests\ElementoRequest;
class ElementoController extends Controller
{
  /**
   * Index view of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \Datatables::collection( Elemento::basicAbm()->get())
                        ->make(true);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('elementos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ElementoRequest $request)
    {
      $input = $request->all();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      return Elemento::create(
        $input
      );
    }

    /**
     * Display the specified resource.
     * @param  Elemento $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Elemento $entity)
    {
      return $entity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Elemento $item)
    {
        return view('elementos.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ElementoRequest $request, Elemento $entity)
    {
       $input = $request->all();
       $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
       $entity->update($input);
       return $entity;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Elemento::find($id)->delete();
    }
}
