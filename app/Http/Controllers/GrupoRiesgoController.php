<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GrupoRiesgo;
use Carbon\Carbon;
use App\Http\Requests\GrupoRiesgoRequest;
class GrupoRiesgoController extends Controller
{
  /**
   * Index view of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      // $items =GrupoRiesgo::allTrashed()
      //                           ->paginate(config('paginate.grupos_riesgo'));
      // return view('grupos_riesgo.index', ['items'=> $items]);
      return \Datatables::collection( GrupoRiesgo::basicAbm()->get())
                          ->make(true);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('grupos_riesgo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GrupoRiesgoRequest $request)
    {
      // dd($input['antecedentes']));
      $input = $request->all();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      return GrupoRiesgo::create($input )
                          ->antecedentes()
                          ->sync($input['antecedentes']);
    }

    /**
     * Display the specified resource.
     * @param  GrupoRiesgo $entity
     * @return \Illuminate\Http\Response
     */
    public function show(GrupoRiesgo $entity)
    {
      return $entity->load('antecedentes');
      // return \App\GrupoRiesgo::withTrashed()
      //                         ->findOrFail(1);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(GrupoRiesgo $item)
    {
        return view('grupos_riesgo.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GrupoRiesgoRequest $request, GrupoRiesgo $entity)
    {
      $input = $request->input();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      $entity->update($input);
      $entity->antecedentes()
             ->sync($input['antecedentes']);
      return redirect()->route('grupos_riesgo.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GrupoRiesgo::find($id)->delete();
    }
}
