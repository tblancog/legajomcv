<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empleado;
use Carbon\Carbon;
use App\Http\Requests\EmpleadoRequest;
class EmpleadoController extends Controller
{
  /**
   * Index view of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      return $items =Empleado::allTrashed()
                                ->paginate(config('paginate.empleados'));

      // return view('empleados.index', ['items'=> $items]);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('empleados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmpleadoRequest $request)
    {
      $input = $request->all();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      Empleado::create(
        $input
      );
      return redirect(route('empleados.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Empleado $item)
    {
        return view('empleados.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmpleadoRequest $request, Empleado $entity)
    {
       $input = $request->all();
       $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
        $entity->update($input);
        return redirect()->route('empleados.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Empleado::find($id)->delete();
    }

    public function show($id)
    {
        return  \App\Empleado::with(['telefonos', 'direcciones'])
                              ->find($id);
    }
}
