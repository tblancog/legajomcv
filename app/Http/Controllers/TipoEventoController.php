<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TipoEvento;
use App\Role;

class TipoEventoController extends Controller
{
  public function list() {
    return TipoEvento::get(['id', 'descripcion as text']);
  }
}
