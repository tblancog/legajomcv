<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EventoRequest;
use App\Repositories\EventoRepository;
use App\Evento;
class EventoController extends Controller
{

  protected $repo;

  public function __construct(EventoRepository $eventRepo){
      $this->repo = $eventRepo;
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(EventoRequest $request) {


    
    return Evento::create(
      $request->all()
    );
  }

  public function show(Evento $entity){
    return $entity;
  }

  public function list(EventoRequest $request) {
    if( $request->has('motivo') ) {
      return $this->repo->listEventsByMotivo( $request->motivo );
    }
    if( $request->has('tipoevento') ) {
      return $this->repo->listEventsByTipoEvento( $request->tipoevento );
    }
    if( $request->has('grupo_derivacion') ) {
      return $this->repo->listEventsByGrupoDerivacion( $request->grupo_derivacion );
    }
    return $this->repo->listEvents();
  }
}
