<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prestador;
use Carbon\Carbon;
use App\Http\Requests\PrestadorRequest;
class PrestadorController extends Controller
{
  /**
   * Index view of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return \Datatables::collection( Prestador::basicAbm()->get())
                        ->make(true);
  }

  public function list(){
    return \App\Prestador::get(['id', 'nombre as text']);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('prestadores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrestadorRequest $request)
    {
      $input = $request->all();
      $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
      return Prestador::create(
        $input
      );
    }

    /**
     * Display the specified resource.
     * @param  Prestador $entity
     * @return \Illuminate\Http\Response
     */
    public function show(Prestador $entity)
    {
      return $entity;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Prestador $item)
    {
        return view('prestadores.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PrestadorRequest $request, Prestador $entity)
    {
       $input = $request->all();
       $input['deleted_at'] = $input['estado'] ? null : Carbon::now();
       $entity->update($input);
       return $entity;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Prestador::find($id)->delete();
    }
}
