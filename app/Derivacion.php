<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Derivacion extends Model
{
    protected $table = 'derivaciones';
    protected $fillable = ['grupo'];
}
