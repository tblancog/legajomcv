<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Evento extends Model
{
    protected $table = 'eventos';
    protected $fillable = [
        'tipo_evento_id', 'user_id', 'consultorio', 'motivo_id', 'fecha_consulta',
        'tipo_diagnostico', 'diagnostico_abuelo_id', 'diagnostico_padre_id',
        'diagnostico_hijo_id', 'evolucion', 'elemento_id', 'derivacion_id',
        'ausentismo', 'dias_ausentismo', 'tipo_intervencion_id', 'informeinicial',
        'n_carpeta', 'detalle_accion', 'fecha_accion', 'estado_id', 'empleado_id',
        'n_siniestro', 'adjunto', 'prestador_id', 'interconsulta',
        'centromedico_id', 'especialidadservicio_id', 'horajornadaaccidente',
        'inicioinasistencialaboral', 'tareahabitualalaccidente',
        'calleocurrenciaaccidente', 'agentematerial_id', 'tiempoexposicionagente',
        'formadiagnostico_id', 'solicitante', 'familiar'
    ];

    public function tipo(){
      return $this->belongsTo(\App\TipoEvento::class, 'tipo_evento_id');
    }

    public function user(){
      return $this->belongsTo(\App\User::class, 'user_id');
    }


    
    public function motivo(){
      return $this->belongsTo(\App\Motivo::class, 'motivo_id');
    }

    public function elemento(){
      return $this->belongsTo(\App\Elemento::class, 'elemento_id');
    }

    public function derivacion(){
      return $this->belongsTo(\App\Derivacion::class, 'derivacion_id');
    }

    public function intervencion(){
      return $this->belongsTo(\App\TipoIntervencion::class, 'tipo_intervencion_id');
    }

    public function estado(){
      return $this->belongsTo(\App\EstadoEvento::class, 'estado_id');
    }

    public function empleado(){
      return $this->belongsTo(\App\Empleado::class, 'empleado_id');
    }

    public function centro(){
      return $this->belongsTo(\App\CentroMedico::class, 'centromedico_id');
    }

    public function agente(){
      return $this->belongsTo(\App\AgenteMaterial::class, 'agentematerial_id');
    }

    public function forma(){
      return $this->belongsTo(\App\FormaDiagnostico::class, 'formadiagnostico_id');
    }

    public function diagnosticoAbuelo(){
      return $this->belongsTo(\App\AbueloDiagnostico::class, 'abuelo_diagnostico_id');
    }

    public function diagnosticoPadre(){
      return $this->belongsTo(\App\PadreDiagnostico::class, 'padre_diagnostico_id');
    }

    public function diagnosticoHijo(){
      return $this->belongsTo(\App\HijoDiagnostico::class, 'hijo_diagnostico_id');
    }

    public function formaAccidente(){
      return $this->belongsTo(\App\formaAccidente::class, 'forma_accidente_id');
    }

    public function scopeListEventos($query){
      return $query->join('motivos', 'eventos.motivo_id', '=', 'motivos.id')
                   ->join('tipos_evento', 'eventos.tipo_evento_id', '=', 'tipos_evento.id')
                   ->limit(20)
                   ->orderBy('fecha_consulta', 'desc');
    }
}
