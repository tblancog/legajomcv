<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';

    public function users() {
      return $this->belongsToMany(\App\User::class, 'user_permissions');
    }

    public function roles() {
      return $this->belongsToMany(\App\Role::class, 'role_permissions');
    }
}
