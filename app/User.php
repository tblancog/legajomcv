<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class User extends Authenticatable
{
    use SoftDeletes, Notifiable;

    protected $table = 'users';
    protected $fillable = ['role_id', 'nombre', 'apellido', 'userad', 'email', 'documento', 'cuil', 'legajo', 'sector', 'puesto', 'gerencia', 'matricula', 'firma', 'is_readonly', 'deleted_at'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    public function getEstadoAttribute (){
      return $this->deleted_at ? false : true;
    }

    public function scopeAllTrashed ($query) {
      return $query->withTrashed()
                   ->orderBy('updated_at', 'desc');
    }

    public function role(){
      return $this->belongsTo(\App\Role::class);
    }

    public function permissions() {
      return $this->belongsToMany(\App\Permission::class, 'user_permissions')
                  ->withPivot('is_readonly');
    }

    public function scopeBasicAbm ($query){
      return $query->select('id', 'email', 'created_at', 'deleted_at')
                  ->addSelect(\DB::raw('CONCAT(nombre, ", ", apellido) as fullname'))
                  ->orderBy('created_at', 'desc')
                  ->orderBy('updated_at', 'desc')
                  ->withTrashed();
    }

    public function scopeFindByKey($query, $key){
      return $query->where('email', $key)
                   ->orWhere('userad', $key)
                   ->orWhere('id', $key)
                   ->withTrashed()
                   ->with(['role',
                           'permissions'=> function($query){
                              $query->select('id', 'name', 'description', 'created_at', 'is_readonly');
                          }]);
    }

    public function scopeSearchLocalUser($query, $q){
       return $query->where('userad', $q)
                    ->orWhere('email', $q)
                    ->orWhere('nombre', 'like', '%'.$q.'%')
                    ->orWhere('apellido', 'like', '%'.$q.'%')
                    ->orWhere('legajo', $q)
                    ->orWhere('documento', $q)
                    ->orWhere('cuil', $q)
                    ->with(['role',
                            'permissions'=> function($query){
                                $query->addSelect('id', 'name', 'description', 'created_at', 'is_readonly');
                    }]);
    }

}
