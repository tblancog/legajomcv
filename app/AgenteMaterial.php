<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgenteMaterial extends Model
{
    protected $table = 'agentes_materiales';

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = $value;
        $this->attributes['codigo'] = str_slug($value);
    }

    public function scopeGetDescription($query){
      return $query->select('id')
                   ->addSelect(\DB::raw('CONCAT(codigo, " - ", descripcion) as text'))
      ;
    }
}
