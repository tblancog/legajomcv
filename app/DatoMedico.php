<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DatoMedico extends Model
{
    use SoftDeletes;

    
    protected $table= 'datos_medicos';

    protected $dates = ['created_at','updated_at','deleted_at'];

    public $timestamps = true;

    protected $fillable= ['empleado_id',
                          'peso',
                          'altura',
                          'grupo_sanguineo',
                          'miembro_superhabil',
                          'dador_sangre',
                          'donante_organos',
                          'certificado_discapacidad',
                          'tipo_discapacidad',
                          'exposicion_riesgo_laboral'
    ];


    public function empleado() {

      return $this->belongsTo(\App\Empleado::class);
    }


}
