<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaDiagnostico extends Model
{
    protected $table = 'formas_diagnostico';
}
