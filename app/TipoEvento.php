<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Motivo;
class TipoEvento extends Model
{
    protected $table = 'tipos_evento';

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function motivos(){
      return $this->belongsToMany(Motivo::class, 'perfil_tipo_motivos');
      // return $this->hasMany(Motivo::class, 'perfil_tipo_motivos');
    }

    public function role(){
      return $this->belongsToMany(Role::class, 'perfil_tipo_motivos', 'tipo_evento_id', 'perfil_id');
    }
}
