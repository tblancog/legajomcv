<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadoEvento extends Model
{
    protected $table = 'estados_evento';

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }
}
