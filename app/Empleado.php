<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Empleado extends Model
{
    use SoftDeletes;

    protected $table = 'empleados';
    protected $dates = ['created_at','updated_at','deleted_at'];
    public $timestamps = true;

    protected $fillable = ['foto', 
                           'nombre',
                           'apellido',
                           'legajo',
                           'dni',
                           'cuil',
                           'genero',
                           'nacionalidad',
                           'estadocivil', 
                           'fecha_ingreso',
                           'fecha_nacimiento',
                           'codigo_puesto',
                           'puesto',
                           'gerencia',
                           'region',
                           'subregion',
                           'perfil',
                           'jefe_directo',
                           'referente_rrhh',
                           'encuadre',
                           'carga_familia',
                           'art',
                           'obra_social_prepaga',
                           'empresa',
                           'domicilio_laboral',
                           'telefono_laboral',
                           'tipo'];
    

   

    public function getEstadoAttribute(){
      return $this->deleted_at ? false : true;
    }

    public function scopeAllTrashed  ($query) {
      return $query->withTrashed()
                   ->orderBy('updated_at', 'desc');
    }

    public function telefonos(){
      return $this->hasMany(\App\Telefono::class);
    }

    public function direcciones(){
      return $this->hasMany(\App\Direccion::class);
    }



    public function datosMedicos() {

      return $this->hasOne(\App\DatoMedico::class);

    }

    public function vacunas(){
      
      return $this->belongsToMany(\App\Vacuna::class,
                                  "datos_medicos_vacunas",
                                  "empleado_id",
                                  "vacuna_id");
    }


    public function antecedentes(){
      
      return $this->belongsToMany(\App\AntecedenteMedico::class,
                                  "datos_medicos_antecedentes",
                                  "empleado_id",
                                  "antecedente_id");
    }


    public function eventos(){
      return $this->hasMany(\App\Evento::class);
    }




    public function addTelefonoAlternativo($telefono) {

    }


    public function addDireccionAlternativa($direccion) {
      
    }
}
