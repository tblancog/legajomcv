<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EspecialidadServicio extends Model
{
    protected $table = 'especialidad_servicios';

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }
}
