<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \App\Diagnostico;

class AbueloDiagnostico extends Diagnostico
{
    protected $table = 'abuelo_diagnosticos';
}
