<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TipoEvento;
use App\Motivo;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name', 'description'];
    protected $dates = ['created_at', 'updated_at'];

    public function user(){
      return $this->hasOne(\App\User::class);
    }

    public function permissions(){
      return $this->belongsToMany(\App\Permission::class, 'role_permissions');
    }

    public function tiposEvento(){
      return $this->belongsToMany(TipoEvento::class, 'perfil_tipo_motivos', 'perfil_id');
    }

    public function motivosConsulta(){
     return $this->belongsToMany(Motivo::class, 'perfil_tipo_motivos', 'perfil_id');
   }
}
