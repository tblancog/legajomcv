<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $table = 'direcciones';

    public function empleado(){
      return $this->belongsTo(\App\Empleado::class);
    }
}
