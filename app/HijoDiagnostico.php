<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HijoDiagnostico extends Model
{
    protected $table = 'hijo_diagnosticos';
}
